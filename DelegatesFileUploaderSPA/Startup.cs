using DelegatesFileUploader.Data;
using DelegatesFileUploader.Services;
using DelegatesFileUploader2.Hubs;
using DelegatesFileUploaderSPA.Services;
using DelegatesFileUploaderSPA.Services.Caspio;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace DelegatesFileUploaderSPA
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSignalR();

            
            services.AddScoped<IFlightUpdater, FlightUpdater>();
            services.AddHostedService<FlightsStatusBackgroundService>();
            services.AddHostedService<ScheduleTimeUpdateBackgroundService>();
            services.AddHttpClient<IAviationEdgeClient, AviationEdgeClient>(cfg =>
            {
                cfg.BaseAddress = new System.Uri("http://aviation-edge.com/");
            });

            services.AddSingleton<IAuth, Auth>();

            services.AddTransient<AuthenticationDelegatingHandler>();

            services.AddScoped<IPassengerListService, PassengerListService>();

            var caspioUrl = Configuration.GetValue<string>("CASPIO_URL");

            services.AddHttpClient<ITablesV2Client, TablesV2Client>(options =>
            {
                options.BaseAddress = new Uri($"{caspioUrl}/rest/");
            })
            .AddHttpMessageHandler<AuthenticationDelegatingHandler>();

            services.AddAuthentication("Basic")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("Basic",o => { 

                });

            services.AddControllersWithViews();
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            var connection = Configuration.GetConnectionString("Default");

            services.AddDbContext<DelegatesContext>
                (options => {
                    options.UseMySql(connection);
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IPassengerListService passengerListService)
        {

            updateDatabase(app, env);

            

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }


            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();
            app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");

                endpoints.MapHub<ImportationProgressHub>("/importprogress");

                endpoints.MapHub<PassengerListProgressHub>("/listprogress");
            });

            //app.UseAuthentication();
            
            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }


        private static void updateDatabase(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<DelegatesContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
