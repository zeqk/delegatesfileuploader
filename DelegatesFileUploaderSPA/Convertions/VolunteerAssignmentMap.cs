﻿using CsvHelper.Configuration;
using DelegatesFileUploaderSPA.Models;
using System;
using System.Globalization;

namespace DelegatesFileUploader.Convertions
{
    public class VolunteerAssignmentMap : ClassMap<VolunteerAssignment>
    {
        public VolunteerAssignmentMap()
        {
            Map(m => m.Congregation);
            Map(m => m.FirstName);
            Map(m => m.GroupsOnBus);
            Map(m => m.ActivityId);
            Map(m => m.LastName);
            Map(m => m.Location);
            Map(m => m.SlotName);
            Map(m => m.End).Ignore(true).ConvertUsing(row =>
            {
                return DateTime.ParseExact(row.GetField("End"), "MMM d yyyy h:mmtt", CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces);
            });
            Map(m => m.Start).Ignore(true).ConvertUsing(row =>
            {
                return DateTime.ParseExact(row.GetField("Start"), "MMM d yyyy h:mmtt", CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces);
            });
        }
    }
}
