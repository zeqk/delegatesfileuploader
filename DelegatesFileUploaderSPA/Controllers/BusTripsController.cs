﻿using DelegatesFileUploader.Convertions;
using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader2.Hubs;
using DelegatesFileUploaderSPA.Models;
using DelegatesFileUploaderSPA.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusTripsController : ControllerBase
    {

        private readonly ILogger _logger;
        private readonly DelegatesContext _delegatesContext;
        private readonly IConfiguration _configuration;

        private readonly IHubContext<ImportationProgressHub> _hubContext;
        private readonly IPassengerListService _passengerListService;

        public BusTripsController(ILogger<BusTripsController> logger, DelegatesContext delegatesContext,
            IConfiguration configuration,
            IHubContext<ImportationProgressHub> hubContext,
            IPassengerListService passengerListService)
        {
            _logger = logger;
            _delegatesContext = delegatesContext;
            _configuration = configuration;
            _hubContext = hubContext;
            _passengerListService = passengerListService;
        }

        [HttpPost("import"), Authorize(Roles = "admin")]
        public async Task<ActionResult<DelegatesViewModel>> Import([Required, AllowedExtensions(new string[] { ".xlsx" })]IFormFile file)
        {
            var rv = new DelegatesViewModel();
            ISheet sheet;
            using (var stream = file.OpenReadStream())
            {

                var wb = WorkbookFactory.Create(stream);
                sheet = wb.GetSheetAt(0);
            }

            await _hubContext.Clients.Group("import").SendAsync("progress", 1);

            try
            {
                var volunteers = new List<Volunteer>();

                var headerRow = sheet.GetRow(0);
                var ic = headerRow.Cells.ToDictionary(c => c.StringCellValue, c => c.ColumnIndex);

                for (int i = 1; i < sheet.PhysicalNumberOfRows; i++)
                {
                    var row = sheet.GetRow(i);
                    var v = row.GetVolunteer(ic);

                    v.PartnerName = v.PartnerName.Trim();
                    v.PartnerName = v.PartnerName != "," ? v.PartnerName : null;


                    volunteers.Add(v);

                    await _hubContext.Clients.Group("import").SendAsync("progress", Decimal.Round(Decimal.Divide(i, sheet.PhysicalNumberOfRows) * 100m, 1));
                }



                var volunteersToDelete = await _delegatesContext.Volunteers.Select(v => new Volunteer() { Id = v.Id }).ToListAsync();

                _delegatesContext.Volunteers.AttachRange(volunteersToDelete);
                _delegatesContext.Volunteers.RemoveRange(volunteersToDelete);

                //_delegatesContext.Volunteers.AddRange(volunteers);

                var partners = volunteers.Where(v => !String.IsNullOrEmpty(v.PartnerName)).ToList();

                foreach (var item in partners)
                {
                    await _hubContext.Clients.Group("import").SendAsync("progress", Decimal.Round(Decimal.Divide(partners.IndexOf(item) + 1, partners.Count) * 100m, 1));
                    item.Partner = volunteers.FirstOrDefault(v => v.Name == item.PartnerName && v.CongregationName == item.CongregationName);
                }

                rv.SuccessfulMessage = $"Se agregaron {volunteers.Count} registros de voluntarios." +
                $" Se asociaron {partners.Count} matrimonios.";
                

                await _delegatesContext.SaveChangesAsync();

                return rv;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost("passengerlist/generate"), Authorize(Roles = "transporte,admin")]
        public async Task<ActionResult> GeneratePassengerListAsync(PSFormData data)
        {
            data.DateFrom = data.DateFrom.Date;
            data.DateTo = data.DateTo.Date.AddDays(1).AddMinutes(-1);

            var fileBytes = await _passengerListService.GetPassengerListFilesAsync(data);

            return File(fileBytes, "application/octet-stream");
        }



    }
}