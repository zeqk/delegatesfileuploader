﻿using CsvHelper;
using DelegatesFileUploader.Convertions;
using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader2.Hubs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DelegateActivitiesController : ControllerBase
    {
        private readonly DelegatesContext _context;
        private readonly IConfiguration _configuration;

        private readonly IHubContext<ImportationProgressHub> _hubContext;

        public DelegateActivitiesController(DelegatesContext context, IConfiguration configuration,
            IHubContext<ImportationProgressHub> hubContext)
        {
            _context = context;
            _configuration = configuration;
            _hubContext = hubContext;
        }
        


        [HttpPost("import"), Authorize(Roles = "admin")]
        public async Task<ActionResult<DelegatesViewModel>> Import([Required]IFormFile file)
        {

            var result = new DelegatesViewModel();

            try
            {
                IEnumerable<Activity> activities = null;


                await _hubContext.Clients.Group("import").SendAsync("progress", 1);

                using (var stream = file.OpenReadStream())
                using (var reader = new StreamReader(stream))
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ";";
                    csv.Configuration.RegisterClassMap<ActivityMap>();
                    activities = csv.GetRecords<Activity>().ToList();
                }

                //var aux = await _context.Delegates.Where(d => d.ApplicantStatus == "Confirmed").ToListAsync();
                //var delegates = aux.ToDictionary(d => d.ApplicantNo);

                //foreach (var item in activities)
                //{
                //    if(delegates.ContainsKey(item.ApplicantNo))
                //        item.Delegate = delegates[item.ApplicantNo];
                //}

                await _hubContext.Clients.Group("import").SendAsync("progress", 50);

                var activitiesOld = await _context.Activities.Select(a => new Activity { Id = a.Id }).ToListAsync();
                _context.Activities.AttachRange(activitiesOld);
                _context.RemoveRange(activitiesOld);

                await _hubContext.Clients.Group("import").SendAsync("progress", 60);



                await _context.Activities.AddRangeAsync(activities);

                


                await _hubContext.Clients.Group("import").SendAsync("progress", 70);

                var importation = new Importation
                {
                    DateTime = DateTime.Now,
                    FileName = file.FileName,
                    RowCount = activities.Count(),
                    SizeMB = Utils.ConvertBytesToMegabytes(file.Length),
                    Type = ImportationTypes.Activities
                };

                await _context.Importations.AddAsync(importation);

                await _hubContext.Clients.Group("import").SendAsync("progress", 80);

                await _context.SaveChangesAsync();

                result.SuccessfulMessage = $"Se agregaron {activities.Count()} registros de actividades.";

                await _hubContext.Clients.Group("import").SendAsync("progress", 100);

            }
            catch (Exception ex)
            {
                result.ErrorMessage = ex.Message + ex.InnerException?.Message;
            }


            result.Init = false;
            return result;
        }

    }
}
