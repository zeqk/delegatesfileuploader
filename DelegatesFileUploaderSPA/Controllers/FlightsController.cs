﻿using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightsController : ControllerBase
    {
        private readonly DelegatesContext _context;
        private readonly IConfiguration _configuration;
        private readonly IFlightUpdater _flightStatusUpdater;

        public FlightsController(DelegatesContext context,
            IFlightUpdater flightStatusUpdater,
            IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            _flightStatusUpdater = flightStatusUpdater;
        }


        [HttpPost("update"), Authorize(Roles = "admin")]
        public async Task<IActionResult> Update([Required,FromBody]FlightsViewModel model)
        {
            await _flightStatusUpdater.UpdateFlightsAsync(model.InitialDate);

            return NoContent();
        }

    }
}
