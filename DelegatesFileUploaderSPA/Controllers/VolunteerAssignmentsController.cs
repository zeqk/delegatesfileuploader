﻿using CsvHelper;
using DelegatesFileUploader.Convertions;
using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader2.Hubs;
using DelegatesFileUploaderSPA.Models;
using DelegatesFileUploaderSPA.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VolunteerAssignmentsController : ControllerBase
    {
        private readonly DelegatesContext _context;
        private readonly IConfiguration _configuration;

        private readonly IHubContext<ImportationProgressHub> _hubContext;

        public VolunteerAssignmentsController(DelegatesContext context, IConfiguration configuration,
            IHubContext<ImportationProgressHub> hubContext)
        {
            _context = context;
            _configuration = configuration;
            _hubContext = hubContext;
        }
        


        [HttpPost("import"), Authorize(Roles = "admin")]
        public async Task<ActionResult<DelegatesViewModel>> Import([Required, AllowedExtensions(new string[] { ".csv" })]IFormFile file)
        {

            var result = new DelegatesViewModel();

            try
            {
                IList<VolunteerAssignment> assignments = null;


                await _hubContext.Clients.Group("import").SendAsync("progress", 1, "Convirtiendo archivo...");

                using (var stream = file.OpenReadStream())
                using (var reader = new StreamReader(stream))
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ";";
                    csv.Configuration.RegisterClassMap<VolunteerAssignmentMap>();
                    assignments = csv.GetRecords<VolunteerAssignment>().ToList();
                }

                await _hubContext.Clients.Group("import").SendAsync("progress", 10, "Limpiando asignaciones anteriores...");

                var old = await _context.VolunteerAssignments.Select(a => new VolunteerAssignment { Id = a.Id }).ToListAsync();
                _context.VolunteerAssignments.AttachRange(old);
                _context.VolunteerAssignments.RemoveRange(old);

                await _context.SaveChangesAsync();

                await _hubContext.Clients.Group("import").SendAsync("progress", 20, "Buscando voluntarios vinculados...");

                
                //foreach (var item in assignments)
                //{
                //    await _hubContext.Clients.Group("import").SendAsync("progress", Decimal.Round(Decimal.Divide(assignments.IndexOf(item) + 1, assignments.Count) * 100m, 1), 
                //        $"Buscando voluntarios vinculados... {item.FirstName} {item.LastName}");

                //    item.Volunteer = await _context.Volunteers.FirstOrDefaultAsync(v => v.FirstName == item.FirstName && v.LastName == item.LastName && item.Congregation.StartsWith(v.CongregationName));
                //}


                await _hubContext.Clients.Group("import").SendAsync("progress", 30, "Agregando asignaciones...");

                await _context.VolunteerAssignments.AddRangeAsync(assignments);

               


                await _hubContext.Clients.Group("import").SendAsync("progress", 70);

                //var importation = new Importation
                //{
                //    DateTime = DateTime.Now,
                //    FileName = file.FileName,
                //    RowCount = assignments.Count(),
                //    SizeMB = Utils.ConvertBytesToMegabytes(file.Length),
                //    Type = ImportationTypes.Activities
                //};

                //await _context.Importations.AddAsync(importation);

                await _hubContext.Clients.Group("import").SendAsync("progress", 80);

                await _context.SaveChangesAsync();

                await UpdateActivities();

                result.SuccessfulMessage = $"Se agregaron {assignments.Count()} registros de asignaciones.";

                await _hubContext.Clients.Group("import").SendAsync("progress", 100);

            }
            catch (Exception ex)
            {
                throw;
            }


            result.Init = false;
            return result;
        }

        async Task UpdateActivities()
        {
            var mold = await _context.MainActivities.Select(a => new MainActivity { Id = a.Id }).ToListAsync();
            _context.MainActivities.AttachRange(mold);
            _context.MainActivities.RemoveRange(mold);

            await _hubContext.Clients.Group("import").SendAsync("progress", 40, "Agregando actividades...");

            var activities = await _context.Activities.ToListAsync();

            var delegateActivitiesByMainActivities = activities.GroupBy(a => new { a.SlotName, a.StartDateTime, a.ActivityName })
                .Select(g => new
                {
                    MainActivity = new MainActivity
                    {
                        SlotName = g.Key.SlotName,
                        Start = g.Key.StartDateTime,
                        ActivityName = g.Key.ActivityName
                    },
                    DelegateActivities = g.ToList()
                }).ToList();




            foreach (var item in delegateActivitiesByMainActivities)
            {
                _context.MainActivities.Add(item.MainActivity);
                foreach (var item2 in item.DelegateActivities)
                {
                    item2.MainActivity = item.MainActivity;
                }
            }



            var assignments = await _context.VolunteerAssignments.ToListAsync();

            var volunteerAssignementsByMainActivities = assignments.GroupBy(a => new { a.SlotName, a.Start, a.ActivityId, a.End, a.Location })
                .Select(g => new
                {
                    MainActivity = new MainActivity
                    {
                        SlotName = g.Key.SlotName,
                        Start = g.Key.Start,
                        ActivityId = g.Key.ActivityId,
                        End = g.Key.End,
                        Location = g.Key.Location
                    },
                    VolunteerAssignements = g.ToList()
                }).ToList();

            foreach (var item in volunteerAssignementsByMainActivities)
            {
                var mainActivity = await _context.MainActivities
                    .FirstOrDefaultAsync(ma => ma.SlotName == item.MainActivity.SlotName
                                                && ma.Start == item.MainActivity.Start);

                if (mainActivity == null)
                {
                    mainActivity = item.MainActivity;
                    _context.MainActivities.Add(mainActivity);
                }
                else
                {
                    mainActivity.ActivityId = item.MainActivity.ActivityId;
                    mainActivity.End = item.MainActivity.End;
                    mainActivity.Location = item.MainActivity.Location;
                    //await _context.MainActivities.up(mainActivity);
                }


                foreach (var item2 in item.VolunteerAssignements)
                {
                    item2.Activity = mainActivity;
                }
            }

            await _context.SaveChangesAsync();
        }

    }
}
