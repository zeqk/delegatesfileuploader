﻿using DelegatesFileUploader.Convertions;
using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader2.Hubs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpecialRoomingController : ControllerBase
    {
        class SpecialRoomingAccommodation
        {
            public string Name { get; set; }
            public string Address { get; set; }
            public string Coordinates { get; set; }
        }


        private readonly DelegatesContext _context;
        private readonly IConfiguration _configuration;

        private readonly IHubContext<ImportationProgressHub> _hubContext;

        public SpecialRoomingController(DelegatesContext context, IConfiguration configuration,
            IHubContext<ImportationProgressHub> hubContext)
        {
            _context = context;
            _configuration = configuration;
            _hubContext = hubContext;
        }



        [HttpPost("import"), Authorize(Roles = "admin")]
        public async Task<ActionResult<DelegatesViewModel>> Index([Required]IFormFile file)
        {

            var result = new DelegatesViewModel();

            try
            {
                

                ISheet sheet;
                using (var stream = file.OpenReadStream())
                {

                    var wb = WorkbookFactory.Create(stream);
                    sheet = wb.GetSheetAt(0);
                }

                var specialRommingList = new List<SpecialRooming>();

                

                for (int i = 1; i < sheet.PhysicalNumberOfRows; i++)
                {
                    try
                    {

                        var specialRooming = sheet.GetRow(i).GetSpecialRooming();
                        if (specialRooming == null)
                            break;
                        specialRommingList.Add(specialRooming);

                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }

                var specialRommingAccommodations = specialRommingList.GroupBy(r => r.AccommodationName).Select(g =>
                new SpecialRoomingAccommodation
                {
                    Name = g.Key,
                    Address = g.FirstOrDefault().AccomodationAddress,
                    Coordinates = g.FirstOrDefault().AccommodationCoordinates
                }).ToList();

                var accomodations = _context.Accommodations.ToList();

                var newAccomodations = new List<Accommodation>();
                
                foreach (var item in specialRommingAccommodations)
                {
                    if (!String.IsNullOrEmpty(item.Name))
                    {

                        var accomodation = accomodations.FirstOrDefault(a => a.Name == item.Name);
                        if (accomodation == null)
                        {
                            accomodation = new Accommodation()
                            {
                                Name = item.Name,
                                Type = AccommodationTypes.Private
                            };
                            newAccomodations.Add(accomodation);
                        }
                        accomodation.Address = item.Address;
                        if (!string.IsNullOrEmpty(item.Coordinates) && item.Coordinates.Contains(','))
                        {
                            var array = item.Coordinates.Split(',');
                            accomodation.Latitude = double.Parse(array[0], CultureInfo.InvariantCulture);
                            accomodation.Longitude = double.Parse(array[1], CultureInfo.InvariantCulture);
                        }
                    }
                }

                await _context.Accommodations.AddRangeAsync(newAccomodations);
                accomodations = _context.Accommodations.ToList();
                accomodations = accomodations.Union(newAccomodations).Distinct().ToList();

                foreach (SpecialRooming item in specialRommingList)
                {
                    var del = _context.Delegates.FirstOrDefault(d => d.ApplicantNo == item.ApplicantNo);
                    if (del == null)
                        throw new Exception("Delegado no encontrado " + item.ApplicantNo);

                    if (!String.IsNullOrWhiteSpace(item.AccommodationName))
                    {

                        del.Accommodation = accomodations.FirstOrDefault(a => a.Name == item.AccommodationName);


                        if (del.Accommodation != null)
                        {
                            del.SpecialRoomingPrivateAccommodation = del.Accommodation.Type == AccommodationTypes.Private;
                        }
                    }
                    
                    
                }

                var importation = new Importation
                {
                    DateTime = DateTime.Now,
                    FileName = file.FileName,
                    RowCount = sheet.PhysicalNumberOfRows,
                    SizeMB = Utils.ConvertBytesToMegabytes(file.Length),
                    Type = ImportationTypes.SpecialRooming
                };

                await _context.Importations.AddAsync(importation);

                await _context.SaveChangesAsync();

                result.SuccessfulMessage = $"Se agregaron {newAccomodations.Count} alojamientos especiales";


            }
            catch (Exception ex)
            {
                result.ErrorMessage = ex.Message + ex.InnerException?.Message;
            }


            result.Init = false;
            return result;
        }

    }
}
