﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class HotelReferenceIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Delegates_HotelName",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "HotelName",
                table: "Delegates");

            migrationBuilder.AlterColumn<string>(
                name: "HotelReference",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_HotelReference",
                table: "Delegates",
                column: "HotelReference");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Delegates_HotelReference",
                table: "Delegates");

            migrationBuilder.AlterColumn<string>(
                name: "HotelReference",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HotelName",
                table: "Delegates",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_HotelName",
                table: "Delegates",
                column: "HotelName");
        }
    }
}
