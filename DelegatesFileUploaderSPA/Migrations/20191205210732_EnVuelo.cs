﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class EnVuelo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData("Fligths", "FlightStatus", "Despegado", "FlightStatus", "EnVuelo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData("Fligths", "FlightStatus", "EnVuelo", "FlightStatus", "Despegado");
        }
    }
}
