﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class NormalizedFlightNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NormalizedFlightNumber",
                table: "Fligths",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fligths_NormalizedFlightNumber",
                table: "Fligths",
                column: "NormalizedFlightNumber");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Fligths_NormalizedFlightNumber",
                table: "Fligths");

            migrationBuilder.DropColumn(
                name: "NormalizedFlightNumber",
                table: "Fligths");
        }
    }
}
