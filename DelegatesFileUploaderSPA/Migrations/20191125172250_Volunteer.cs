﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class Volunteer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Volunteers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Approved = table.Column<bool>(nullable: false),
                    Helper = table.Column<bool>(nullable: false),
                    Rights = table.Column<string>(nullable: true),
                    CongregationCity = table.Column<string>(nullable: true),
                    CongregationName = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    PartnerName = table.Column<string>(nullable: true),
                    IsAssigned = table.Column<bool>(nullable: false),
                    PartnerId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Volunteers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Volunteers_Volunteers_PartnerId",
                        column: x => x.PartnerId,
                        principalTable: "Volunteers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Volunteers_PartnerId",
                table: "Volunteers",
                column: "PartnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Volunteers");
        }
    }
}
