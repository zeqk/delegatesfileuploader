﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class TextFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ArrivalSettedText",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ArrivalTimeText",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepartureSettedText",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepartureTimeText",
                table: "Delegates",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArrivalSettedText",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "ArrivalTimeText",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "DepartureSettedText",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "DepartureTimeText",
                table: "Delegates");
        }
    }
}
