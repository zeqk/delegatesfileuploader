﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class FligthsAirline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Airline",
                table: "Fligths",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Departure",
                table: "Fligths",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Airline",
                table: "Fligths");

            migrationBuilder.DropColumn(
                name: "Departure",
                table: "Fligths");
        }
    }
}
