﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class CheckInHourAndIndexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "HotelName",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DepartureAirport",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DepartureAirline",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ArrivalAirport",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ArrivalAirline",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CheckInHour",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CheckOutHour",
                table: "Delegates",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_ApplicantGroupNo",
                table: "Delegates",
                column: "ApplicantGroupNo");

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_ApplicantNo",
                table: "Delegates",
                column: "ApplicantNo");

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_ArrivalAirline",
                table: "Delegates",
                column: "ArrivalAirline");

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_ArrivalAirport",
                table: "Delegates",
                column: "ArrivalAirport");

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_DepartureAirline",
                table: "Delegates",
                column: "DepartureAirline");

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_DepartureAirport",
                table: "Delegates",
                column: "DepartureAirport");

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_HotelName",
                table: "Delegates",
                column: "HotelName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Delegates_ApplicantGroupNo",
                table: "Delegates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_ApplicantNo",
                table: "Delegates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_ArrivalAirline",
                table: "Delegates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_ArrivalAirport",
                table: "Delegates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_DepartureAirline",
                table: "Delegates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_DepartureAirport",
                table: "Delegates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_HotelName",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "CheckInHour",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "CheckOutHour",
                table: "Delegates");

            migrationBuilder.AlterColumn<string>(
                name: "HotelName",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DepartureAirport",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DepartureAirline",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ArrivalAirport",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ArrivalAirline",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
