﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class VolunteerFirtname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Volunteers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Volunteers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Volunteers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Volunteers");
        }
    }
}
