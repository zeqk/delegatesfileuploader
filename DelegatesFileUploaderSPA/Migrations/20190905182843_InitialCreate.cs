﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DelegatesFileUploader.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Delegates",
                columns: table => new
                {
                    ApplicantNo = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApplicantGroupNo = table.Column<long>(nullable: false),
                    ApplicantStatus = table.Column<long>(nullable: false),
                    ArrivalAirline = table.Column<string>(nullable: true),
                    ArrivalAirport = table.Column<string>(nullable: true),
                    ArrivalDay = table.Column<string>(nullable: true),
                    ArrivalFlight = table.Column<string>(nullable: true),
                    ArrivalTime = table.Column<string>(nullable: true),
                    ArrivalDateTime = table.Column<string>(nullable: true),
                    AvailableDays = table.Column<int>(nullable: false),
                    AvailableFrom = table.Column<string>(nullable: true),
                    AvailableFromDate = table.Column<DateTime>(nullable: true),
                    AvailableTo = table.Column<string>(nullable: true),
                    AvailableToDate = table.Column<DateTime>(nullable: true),
                    BranchName = table.Column<string>(nullable: true),
                    CheckInDateTime = table.Column<DateTime>(nullable: true),
                    CheckOutDateTime = table.Column<DateTime>(nullable: true),
                    DepartureAirline = table.Column<string>(nullable: true),
                    DepartureAirport = table.Column<string>(nullable: true),
                    DepartureDay = table.Column<string>(nullable: true),
                    DepartureFlight = table.Column<string>(nullable: true),
                    DepartureTime = table.Column<string>(nullable: true),
                    DepartureTransport = table.Column<string>(nullable: true),
                    Dietary = table.Column<string>(nullable: true),
                    EdiStatus = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    GroupArrival = table.Column<string>(nullable: true),
                    GroupDeparture = table.Column<string>(nullable: true),
                    HotelName = table.Column<string>(nullable: true),
                    IndividualArrival = table.Column<DateTime>(nullable: true),
                    IndividualDeparture = table.Column<DateTime>(nullable: true),
                    IsGroupLeader = table.Column<bool>(nullable: false),
                    Language = table.Column<string>(nullable: true),
                    Schedule = table.Column<int>(nullable: false),
                    SpecialRooming = table.Column<bool>(nullable: false),
                    IsSpecialGuest = table.Column<bool>(nullable: false),
                    ConfirmedDate = table.Column<DateTime>(nullable: true),
                    SelectedDate = table.Column<DateTime>(nullable: true),
                    Activity = table.Column<string>(nullable: true),
                    ForeignServant = table.Column<bool>(nullable: false),
                    SFTS = table.Column<bool>(nullable: false),
                    ArrivalSetted = table.Column<bool>(nullable: false),
                    DepartureSetted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Delegates", x => x.ApplicantNo);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Delegates");
        }
    }
}
