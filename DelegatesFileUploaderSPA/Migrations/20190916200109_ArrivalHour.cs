﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class ArrivalHour : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArrivalTimeText",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "DepartureTimeText",
                table: "Delegates");

            migrationBuilder.AddColumn<int>(
                name: "ArrivalHour",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DepartureHour",
                table: "Delegates",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArrivalHour",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "DepartureHour",
                table: "Delegates");

            migrationBuilder.AddColumn<string>(
                name: "ArrivalTimeText",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepartureTimeText",
                table: "Delegates",
                nullable: true);
        }
    }
}
