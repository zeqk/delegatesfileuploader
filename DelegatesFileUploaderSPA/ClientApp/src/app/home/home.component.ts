import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpEvent, HttpEventType, HttpClient } from '@angular/common/http';
import { FileUploadService } from '../file-upload.service';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

    form: FormGroup;
    progress: number = 0;
    progress2: number = 0;
    progressMessage: string = null;

    processing = false;
    result: any;
    fileName: string = '.xlsx';

    constructor(
        public fb: FormBuilder,
        public fileUploadService: FileUploadService,
        private http: HttpClient,
        @Inject('BASE_URL') private baseUrl: string
    ) {
        this.form = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            entity: [null, Validators.required],
            file: [null, Validators.required]
        });
    }

    ngOnInit() { }

    uploadFile(event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.form.patchValue({
            file: file
        });
        this.form.get('file').updateValueAndValidity()
    }

    importFile() {

        

        this.result = null;
        this.processing = true;
        this.progress = 0;
        this.progress2 = 0;
        this.form.disable();

        let hubConnection = new HubConnectionBuilder()
            .withUrl(this.baseUrl + 'importprogress')
            .build();

        hubConnection.start()
            .then(() => {
                hubConnection.invoke('AssociateJob', 'import');
            })
            .catch(err => {
                console.log(err);
            });

        hubConnection.on('progress', (data: number, progressMessage: string) => {
            this.progress2 = data;
            this.progressMessage = progressMessage;
        });

        this.fileUploadService.importFile(
            this.form.value.file,
            this.form.value.entity,
            this.form.value.username,
            this.form.value.password
        ).subscribe((event: HttpEvent<any>) => {
            switch (event.type) {
                case HttpEventType.Sent:
                    console.log('Request has been made!');
                    break;
                case HttpEventType.ResponseHeader:
                    console.log('Response header has been received!');
                    break;
                case HttpEventType.UploadProgress:

                    this.progress = Math.round(event.loaded / event.total * 100);
                    console.log(`Uploaded! ${this.progress}%`);
                    break;
                case HttpEventType.Response:
                    
                    this.result = event.body;
                    console.log('Importación exitosa!', event.body);
                    hubConnection.stop();
                    this.form.enable();
                    this.processing = false

                    //setTimeout(() => {
                    //    this.progress = 0;
                    //    this.progress2 = 0;
                    //}, 1500);

            }
        },
        error => {
            hubConnection.stop();

            this.processing = false
            this.form.enable();
            this.result = {
                ErrorMessage:  JSON.stringify(error)
            };
        });
    }

    selectOption(entity: string) {

        console.log('selectOption', entity);
        switch (entity) {

            case 'delegates':
                this.fileName = 'Delegate Filter*.xlsx | *.xlsx';
                break;
            case 'delegates':
                this.fileName = 'All Delegate Activities*.csv |  *.csv';
                break;
            default:
        }


    }

}
