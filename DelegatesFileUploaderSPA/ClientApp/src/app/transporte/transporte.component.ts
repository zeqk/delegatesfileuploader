import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpEvent, HttpEventType, HttpClient } from '@angular/common/http';
import { FileUploadService } from '../file-upload.service';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { PMFormData } from '../pm-form-data.model';

@Component({
  selector: 'app-transporte',
  templateUrl: './transporte.component.html',
  styleUrls: ['./transporte.component.css']
})
export class TransporteComponent implements OnInit {
    

    form: FormGroup;
    progress: number = 0;
    progressMessage: string = null;

    processing = false;
    result: any;
    fileName: string = '.xlsx';

    constructor(
        public fb: FormBuilder,
        private http: HttpClient,
        private fileUploadService: FileUploadService,
        @Inject('BASE_URL') private baseUrl: string
    ) {
        this.form = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
            dateFrom: ['', Validators.required],
            dateTo: ['', Validators.required],
            name: ['', Validators.required],
            cuit: ['', Validators.required],
            address: ['', Validators.required],
        });
    }

    ngOnInit(): void {
    }


    generatePassengerList() {



        this.result = null;
        this.processing = true;
        this.progress = 0;
        this.progressMessage = "";
        this.form.disable();

        let hubConnection = new HubConnectionBuilder()
            .withUrl(this.baseUrl + 'listprogress')
            .build();

        hubConnection.start()
            .then(() => {
                hubConnection.invoke('AssociateJob', 'list');
            })
            .catch(err => {
                console.log(err);
            });

        hubConnection.on('progress', (data: number, progressMessage: string) => {
            this.progress = data;
            this.progressMessage = progressMessage;
        });

        var data: PMFormData =  {
            name: this.form.value.name,
            address: this.form.value.address,
            cuit: this.form.value.cuit,
            dateFrom: this.form.value.dateFrom,
            dateTo: this.form.value.dateTo,
        };


        this.fileUploadService.generatePassengerList(data,
            this.form.value.username,
            this.form.value.password).subscribe(blob => {

                hubConnection.stop();
                this.form.enable();
                this.processing = false;

                let a = <any>document.createElement("a");
                document.body.appendChild(a);
                (a).style = "display: none";
                const url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = 'ListasPasajerosCNRT.zip';
                a.click();

                ////let blob:any = new Blob([response.blob()], { type: 'text/json; charset=utf-8' });
                //const url = window.URL.createObjectURL(blob);
                //window.open(url);
                ////window.location.href = response.url;
                ////fileSaver.saveAs(blob, 'employees.json');
            }, error => {
                this.form.enable();
                this.processing = false;
                this.result = {
                    ErrorMessage: JSON.stringify(error)
                };

                hubConnection.stop();
            });
    }


}
