export class PMFormData {
    name: string;
    cuit: string;
    address: string;
    dateFrom: Date;
    dateTo: Date;
}
