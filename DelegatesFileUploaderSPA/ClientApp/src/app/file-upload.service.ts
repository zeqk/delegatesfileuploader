import { Injectable, Inject } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { PMFormData } from './pm-form-data.model';

@Injectable({
    providedIn: 'root'
})
export class FileUploadService {

    constructor(private http: HttpClient, @Inject('BASE_URL')  private baseUrl: string) { }

    importFile(file: File, path: string, username: string, password: string): Observable<any> {
        var formData: any = new FormData();
        formData.append("file", file);

        return this.http.post(this.baseUrl + `api/${path}/import`, formData, {
            reportProgress: true,
            observe: 'events',
            headers: { 'Authorization': 'Basic ' + window.btoa(username + ':' + password) }
        });

        //    .pipe(
        //    catchError(this.errorMgmt)
        //);
    }


    generatePassengerList(pmFormData: PMFormData, username: string, password: string): Observable<Blob> {

        return this.http.post(this.baseUrl + `api/bustrips/passengerlist/generate`, JSON.stringify(pmFormData), {
            headers: {
                'Authorization': 'Basic ' + window.btoa(username + ':' + password),
                'Content-Type': 'application/json'
            },
            responseType: 'blob' 
        });
    }

    errorMgmt(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }


}
