﻿using System;

namespace DelegatesFileUploader.Models
{
    public class Flight
    {
        public long Id { get; set; }

        public string FlightNumber { get; set; }

        public string NormalizedFlightNumber { get; set; }

        public string Airport { get; set; }

        public DateTime FlightDateTime { get; set; }

        public DateTime JWFlightDateTime { get; set; }

        public DateTime? EstimatedDateTime { get; set; }

        public DateTime? ActualDateTime { get; set; }

        public DateTime? EstimatedRunway { get; set; }

        public DateTime? ActualRunway { get; set; }

        public int DelayedMinutes { get; set; }

        public int FlightHour { get; set; }

        public int JWFlightHour { get; set; }

        public string FlightURL { get; set; }

        public string Airline { get; set; }

        public string Status { get; set; }

        public FlightStatuses? FlightStatus { get; set; }

        public bool Departure { get; set; }

        public DateTime? UpdateStatusDateTime { get; set; }
    }
}
