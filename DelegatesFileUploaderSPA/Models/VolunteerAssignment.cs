﻿using System;

namespace DelegatesFileUploaderSPA.Models
{
    public class VolunteerAssignment
    {
        public long Id { get; set; }

        public long ActivityId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        /// <summary>
        /// Ej. Norte, Ramos Mejía, Buenos Aires (Ramos Mejía)
        /// </summary>
        public string Congregation { get; set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string SlotName { get; set; }
        public string Location { get; set; }

        public int GroupsOnBus { get; set; }

        public string Usage { get; set; }

        public Volunteer Volunteer { get; set; }

        public MainActivity Activity { get; set; }
    }
}
