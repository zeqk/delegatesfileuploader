﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Models
{
    public class MainActivity
    {
        public int Id { get; set; }

        public long ActivityId { get; set; }

        public string ActivityName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public string SlotName { get; set; }
        public string Location { get; set; }
    }
}
