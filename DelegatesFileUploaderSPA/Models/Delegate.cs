﻿using System;

namespace DelegatesFileUploader.Models
{
    public class Delegate
    {
        public long ApplicantGroupNo { get; set; }
        public long ApplicantNo { get; set; }
        public string Name { get; set; }
        public string ApplicantStatus { get; set; }
        public string ArrivalAirline { get; set; }
        public string ArrivalAirport { get; set; }
        public string ArrivalTransport { get; set; }
        public int? ArrivalHour { get; set; }
        public string ArrivalDay { get; set; }
        public string ArrivalFlight { get; set; }
        public DateTime? ArrivalDateTime { get; set; }

        public Flight ArrivalFlightData { get; set; }

        public int AvailableDays { get; set; }
        public string AvailableFrom { get; set; }
        public DateTime? AvailableFromDate { get; set; }
        public string AvailableTo { get; set; }
        public DateTime? AvailableToDate { get; set; }
        public string BranchName { get; set; }
        public DateTime? CheckInDateTime { get; set; }
        public int? CheckInHour { get; set; }
        public DateTime? CheckOutDateTime { get; set; }
        public int? CheckOutHour { get; set; }
        public string DepartureAirline { get; set; }
        public string DepartureAirport { get; set; }
        public int? DepartureHour { get; set; }
        public string DepartureDay { get; set; }
        public string DepartureFlight { get; set; }
        public DateTime? DepartureDateTime { get; set; }
        public string DepartureTransport { get; set; }

        public Flight DepartureFlightData { get; set; }
        public string Dietary { get; set; }
        public string EdiStatus { get; set; }
        public bool FromRestrictedCountry { get; set; }
        public string Gender { get; set; }
        public string GroupArrival { get; set; }
        public DateTime? GroupArrivalDateTime { get; set; }
        public string GroupDeparture { get; set; }
        public DateTime? GroupDepartureDateTime { get; set; }
        public string HotelReference { get; set; }
        public string IndividualArrival { get; set; }
        public DateTime? IndividualArrivalDateTime { get; set; }
        public string IndividualDeparture { get; set; }
        public DateTime? IndividualDepartureDateTime { get; set; }
        public bool IsGroupLeader { get; set; }
        public string Language { get; set; }
        public int? Schedule { get; set; }
        public bool SpecialRooming { get; set; }
        public bool IsSpecialGuest { get; set; }
        public DateTime? ConfirmedDate { get; set; }
        public DateTime? SelectedDate { get; set; }
        public string Activity { get; set; }
        public bool ForeignServant { get; set; }
        public bool SFTS { get; set; }
        public bool ArrivalSetted { get; set; }
        public string ArrivalSettedText { get; set; }
        public bool DepartureSetted { get; set; }
        public string DepartureSettedText { get; set; }
        public DateTime LastChangeDate { get; set; }

        public bool ArrivalAndCheckingSameDay { get; set; }

        public bool DepartureAndCheckoutSameDay { get; set; }

        public bool SpecialRoomingPrivateAccommodation { get; set; }
        public Accommodation Accommodation { get; set; }

    }


}
