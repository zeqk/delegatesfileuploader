﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Models
{
    public class Volunteer
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Approved { get; set; }
        public bool Helper { get; set; }

        public string Rights { get; set; }

        public string CongregationCity { get; set; }

        public string CongregationName { get; set; }

        public string Gender { get; set; }

        public string PartnerName { get; set; }

        public bool IsAssigned { get; set; }

        public Volunteer Partner { get; set; }
    }
}
