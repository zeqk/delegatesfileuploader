﻿namespace DelegatesFileUploader.Models
{
    public enum FlightStatuses
    {
        Arribado,
        Programado,
        Cancelado,
        EnVuelo,
        Incidente,
        Desviado,
        Redirigido,
        Desconocido
    }
}
