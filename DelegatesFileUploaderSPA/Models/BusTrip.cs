﻿using System;

namespace DelegatesFileUploaderSPA.Models
{
    public class BusTrip
    {
        public long ActivityId { get; set; }
        public string ActivityName { get; set; }
        public string SlotName { get; set; }
        //public string Location { get; set; }
        public DateTime StartTime { get; set; }
        //public string ReturnTime { get; set; }
        public DateTime EndTime { get; set; }
        //public string Delegates { get; set; }
        //public string GroupsOnBus { get; set; }
        public DateTime SlotTime { get; set; }
        //public string AnnouncedTime { get; set; }
        public string Hotels { get; set; }
        public string PickupLocation { get; set; }
        //public string H562 { get; set; }
        //public string H725 { get; set; }
        //public string Abasto { get; set; }
        //public string ArgentaTower { get; set; }
        //public string Betel { get; set; }
        //public string Bristol { get; set; }
        //public string Broadway { get; set; }
        //public string Centuria { get; set; }
        //public string CHAmericas { get; set; }
        //public string CHMadero { get; set; }
        //public string Conquistador { get; set; }
        //public string Conte { get; set; }
        //public string CyanAmericas { get; set; }
        //public string DazzlerMaipu { get; set; }
        //public string DazzlerSanMartin { get; set; }
        //public string HiltonBuenosAires { get; set; }
        //public string Libertador { get; set; }
        //public string MeliáBuenosAires { get; set; }
        //public string NH9deJulio { get; set; }
        //public string NHBuenosAiresCity { get; set; }
        //public string NHCentroHistórico { get; set; }
        //public string NHCrillon { get; set; }
        //public string NHFlorida { get; set; }
        //public string NHJousten { get; set; }
        //public string NHLancaster { get; set; }
        //public string NHLATINO { get; set; }
        //public string NHTango { get; set; }
        //public string OwnRecoleta { get; set; }
        //public string Panamericano { get; set; }
        //public string ParkSilver { get; set; }
        //public string ParkTower { get; set; }
        //public string Pestana { get; set; }
        //public string PrincipadoDowntown { get; set; }
        //public string RecoletaHotel { get; set; }
        //public string RecoletaSuites { get; set; }
        //public string Regal { get; set; }
        //public string Regente { get; set; }
        //public string RepúblicaWellness { get; set; }
        //public string Savoy { get; set; }
        //public string Scala { get; set; }
        //public string Sheraton { get; set; }
        //public string Two { get; set; }

    }
}
