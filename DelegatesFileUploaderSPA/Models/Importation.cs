﻿using System;

namespace DelegatesFileUploader.Models
{
    public class Importation
    {
        public long Id { get; set; }
        public string FileName { get; set; }

        public DateTime DateTime { get; set; }

        public long RowCount { get; set; }

        public double SizeMB { get; set; }

        public ImportationTypes Type { get; set; }

    }
}
