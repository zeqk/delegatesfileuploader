﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DelegatesFileUploader.Models.AviationEdge;

namespace DelegatesFileUploader.Services
{
    public interface IAviationEdgeClient
    {
        Task<IList<FlightTimetable>> GetTimetablesAsync(string airportIATA, FlightDirections flightDirection);
    }
}