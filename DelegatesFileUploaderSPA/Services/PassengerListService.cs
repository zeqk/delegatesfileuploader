﻿using DelegatesFileUploader.Data;
using DelegatesFileUploader2.Hubs;
using DelegatesFileUploaderSPA.Properties;
using DelegatesFileUploaderSPA.Services.Caspio;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Services
{
    public class PassengerListService : IPassengerListService
    {
        private readonly ITablesV2Client _tablesV2Client;
        private readonly IHubContext<PassengerListProgressHub> _hubContext;
        private readonly DelegatesContext _context;

        public PassengerListService(ITablesV2Client tablesV2Client,
            DelegatesContext context,
            IHubContext<PassengerListProgressHub> hubContext)
        {
            _tablesV2Client = tablesV2Client;
            _hubContext = hubContext;
            _context = context;
        }

        public async Task<byte[]> GetPassengerListFilesAsync(PSFormData data)
        {
            var formsData = await GetPassengerListAsync(data);

            var templateFile = new MemoryStream();
            templateFile.Write(Resources.LISTA_DE_PASAJEROS_CNRT_2019, 0, Resources.LISTA_DE_PASAJEROS_CNRT_2019.Length);


            var zipStream = new MemoryStream();
            using (var archive = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
            {
                foreach (var form in formsData)
                {
                    using (var p = new ExcelPackage(templateFile))
                    {

                        //Get the Worksheet created in the previous codesample. 
                        var ws = p.Workbook.Worksheets.FirstOrDefault();

                        setData(ws, form);

                        var entry = archive.CreateEntry($"{form.Id.Replace("/"," ")}.xlsx");

                        using (var entryStream = entry.Open())
                        {
                            p.SaveAs(entryStream);
                        }

                    }
                } 
                    
            }
            return zipStream.ToArray();
        }

        private void setData(ExcelWorksheet worksheet, PlusmarForm form)
        {
            worksheet.Cells["C18"].Value = form.Details;
            worksheet.Cells["C8"].Value = form.Name;
            worksheet.Cells["C10"].Value = form.CUIT;
            worksheet.Cells["C12"].Value = form.Address;


            for (int i = 0; i < form.Passengers.Count; i++)
            {
                var rowNumber = i + 27;
                var passenger = form.Passengers[i];

                worksheet.Cells["B" + rowNumber.ToString()].Value = passenger.Lastname;
                worksheet.Cells["C" + rowNumber.ToString()].Value = passenger.Firstname;
                worksheet.Cells["D" + rowNumber.ToString()].Value = passenger.LegalIdType;
                worksheet.Cells["E" + rowNumber.ToString()].Value = passenger.LegalIdNumber;
                worksheet.Cells["F" + rowNumber.ToString()].Value = passenger.UnderAge;
                worksheet.Cells["G" + rowNumber.ToString()].Value = passenger.Nationality;
                worksheet.Cells["H" + rowNumber.ToString()].Value = passenger.Gender;
            }
        }

        public async Task<IList<PlusmarForm>> GetPassengerListAsync(PSFormData data)
        {

            await _hubContext.Clients.Group("list").SendAsync("progress", 2, "Obteniendo actividades...");

            var activities = await _context.Activities
            .Where(a => a.StartDateTime >= data.DateFrom && a.StartDateTime <= data.DateTo && a.SlotName.StartsWith("B"))
            .ToListAsync();
                
            var delegatesByActivities = activities.GroupBy(a => new { a.SlotName, a.ActivityName })
                .Select(g => new
                {
                    g.Key.SlotName,
                    StartDateTime = g.Min(a => a.StartDateTime),
                    g.Key.ActivityName,
                    Delegates = g.Select(a => a.ApplicantNo).ToList()
                }
                ).ToList();


            await _hubContext.Clients.Group("list").SendAsync("progress", 6, "Obteniendo lista de paises de Caspio...");

            var aux = (await _tablesV2Client.GetRecordsAsync("Countries", null, null, null, null, null, null, 1000)).Result;
            var caspioCountries = aux.ToDictionary(dict => dict["Alpha3"]);

            await _hubContext.Clients.Group("list").SendAsync("progress", 8, "Obteniendo delegados...");

            var delegates = await _context.Delegates.Where(d => d.ApplicantStatus == "Confirmed").ToListAsync();
            var delegatesByApplicantNo = delegates.ToDictionary(d => d.ApplicantNo);

            await _hubContext.Clients.Group("list").SendAsync("progress", 10, "Obteniendo pasaportes de delegados...");

            var delegatesExtraData = await getDelegatesExtraDataAsync();

            await _hubContext.Clients.Group("list").SendAsync("progress", 12, "Obteniendo transfers...");

            var transfers = await getTransfersAsync();

            await _hubContext.Clients.Group("list").SendAsync("progress", 14, "Obteniendo asignaciones de voluntarios...");

            var volunteerAssignments = await getVolunteersAssignmentsBySlotNameAsync();

            var forms = new List<PlusmarForm>();

            //Buses para actividades

            foreach (var activity in delegatesByActivities)
            {

                await _hubContext.Clients.Group("list").SendAsync("progress", Decimal.Round(Decimal.Divide(delegatesByActivities.IndexOf(activity), delegatesByActivities.Count) * 50m, 1), $"Generando lista para {activity.SlotName}");

                List<IDictionary<string,object>> volunteers = null;

                if (volunteerAssignments.ContainsKey(activity.SlotName))
                    volunteers = volunteerAssignments[activity.SlotName];

                var form = new PlusmarForm
                {
                    Passengers = new List<Passenger>(),
                };

                var passengersCount = activity.Delegates.Count;

                foreach (var ApplicantNo in activity.Delegates)
                {
                    if (delegatesByApplicantNo.ContainsKey(ApplicantNo))
                    {

                        var d = delegatesByApplicantNo[ApplicantNo];
                        var names = d.Name.Split(',');
                        var firstName = names[1];
                        var lastName = names[0];

                        var passenger = new Passenger
                        {
                            Firstname = firstName,
                            Lastname = lastName,
                            Gender = d.Gender.First().ToString(),
                            LegalIdType = "PS",
                            UnderAge = "No",
                            Nationality = d.BranchName

                        };

                        if (delegatesExtraData.ContainsKey(ApplicantNo))
                        {

                            var caspioData = delegatesExtraData[ApplicantNo];
                            var country = caspioData["IssuerCountryID"].ToString();
                            if (caspioCountries.ContainsKey(country))
                                country = caspioCountries[country]["CountryName_ES"].ToString();

                            passenger.LegalIdNumber = caspioData["PassportNo"].ToString();
                            passenger.Nationality = country;
                        }

                        form.Passengers.Add(passenger);
                    }

                }

                if (volunteers != null)
                {
                    passengersCount += volunteers.Count;
                    foreach (var v in volunteers)
                    {
                                
                        var passenger = new Passenger
                        {
                            Firstname = v["FirstName"].ToString(),
                            Lastname = v["LastName"].ToString(),
                            UnderAge = "No"

                        };
                        form.Passengers.Add(passenger);
                    }
                }

                form.Id = $"{activity.StartDateTime.ToString("yyyy.MM.dd HH.mm")} {activity.SlotName} {activity.ActivityName}";
                form.Details = $"Servicio {activity.SlotName} | {passengersCount} pasajeros | Fecha de salida: {activity.StartDateTime.ToString("dd-MM-yyyy HH:mm")}";
                form.Name = data.Name;
                form.CUIT = data.CUIT;
                form.Address = data.Address;
                forms.Add(form);
            }

            //Buses de transfers
            var ti = 0;

            foreach (var transfer in transfers)
            {

                await _hubContext.Clients.Group("list").SendAsync("progress", Decimal.Round(Decimal.Divide(ti, transfers.Count) * 50m, 1) + 50, $"Generando lista para {transfer.Key}");
                ti++;

                List<IDictionary<string, object>> volunteers = null;

                if (volunteerAssignments.ContainsKey(transfer.Key))
                    volunteers = volunteerAssignments[transfer.Key];

                var form = new PlusmarForm
                {
                    Passengers = new List<Passenger>(),
                };

                var passengersCount = transfer.Value.Count;

                foreach (var ApplicantNo in transfer.Value)
                {

                    if (delegatesByApplicantNo.ContainsKey(ApplicantNo))
                    {
                        var d = delegatesByApplicantNo[ApplicantNo];

                        if (delegatesExtraData.ContainsKey(ApplicantNo))
                        {

                            var caspioData = delegatesExtraData[ApplicantNo];
                            var country = caspioData["IssuerCountryID"].ToString();
                            if (caspioCountries.ContainsKey(country))
                                country = caspioCountries[country]["CountryName_ES"].ToString();
                            var passenger = new Passenger
                            {
                                Firstname = caspioData["FirstName"].ToString(),
                                Lastname = caspioData["LastName"].ToString(),
                                Gender = d.Gender.First().ToString(),
                                LegalIdNumber = caspioData["PassportNo"].ToString(),
                                LegalIdType = "Pasaporte",
                                Nationality = country,
                                UnderAge = "No"

                            };
                            form.Passengers.Add(passenger);
                        }
                    }

                }

                if (volunteers != null)
                {
                    passengersCount += volunteers.Count;
                    foreach (var v in volunteers)
                    {

                        var passenger = new Passenger
                        {
                            Firstname = v["FirstName"].ToString(),
                            Lastname = v["LastName"].ToString(),
                            UnderAge = "No"

                        };
                        form.Passengers.Add(passenger);
                    }
                }
                form.Id = transfer.Key;
                form.Details = $"Servicio {transfer.Key} | {passengersCount} pasajeros";// | Fecha de salida: {activity.StartDateTime.ToString("dd-MM-yyyy HH:mm")}";
                form.Name = data.Name;
                form.CUIT = data.CUIT;
                form.Address = data.Address;
                forms.Add(form);
            }

            await _hubContext.Clients.Group("list").SendAsync("progress", 100, $"Finalizado");

            return forms;

            
        }


        private async Task<IDictionary<long,IDictionary<string,object>>> getDelegatesExtraDataAsync()
        {
            var aux = new List<IDictionary<string, object>>();
            var count = 0;
            var page = 1;
            do
            {

                var caspioResult = (await _tablesV2Client.GetRecordsAsync("DelegatesExtraData", null, null, null, null, null, page, 1000)).Result;
                aux.AddRange(caspioResult);
                page++;
                count = caspioResult.Count;

            } while (count == 1000);

            var rv = aux.ToDictionary(dict => Convert.ToInt64(dict["ApplicantNo"]));

            return rv;
        }

        private async Task<IDictionary<string, List<IDictionary<string, object>>>> getVolunteersAssignmentsBySlotNameAsync()
        {
            var aux = new List<IDictionary<string, object>>();
            var count = 0;
            var page = 1;
            do
            {

                var caspioResult = (await _tablesV2Client.GetRecordsAsync("VolunteersAssignments", "SlotName,FirstName,LastName,FullName", "SlotName like 'B%' OR SlotName like 'T%'", null, null, null, page, 1000)).Result;
                aux.AddRange(caspioResult);
                page++;
                count = caspioResult.Count;

            } while (count == 1000);

            var rv = aux.GroupBy(dict => dict["SlotName"].ToString())
                .ToDictionary
                (
                    g => g.Key, 
                    g => g.GroupBy(g2 => g2["FullName"]).Select(g2 => g2.FirstOrDefault()).ToList()
                );

            return rv;
        }

        private async Task<IDictionary<string, List<long>>> getTransfersAsync()
        {
            var aux = new List<IDictionary<string, object>>();
            var count = 0;
            var page = 1;
            do
            {

                var caspioResult = (await _tablesV2Client.GetRecordsAsync("Delegates", "ApplicantNo,AirportTransferSlotName", "AirportTransferAssigned = 'Yes' AND AirportTransferSlotName like 'T%'", null, null, null, page, 1000)).Result;
                aux.AddRange(caspioResult);
                page++;
                count = caspioResult.Count;

            } while (count == 1000);

            var rv = aux.GroupBy(dict => dict["AirportTransferSlotName"].ToString())
                .ToDictionary
                (
                    g => g.Key,
                    g => g.Select(d => Convert.ToInt64(d["ApplicantNo"])).ToList()
                );

            return rv;
        }
    }



}
