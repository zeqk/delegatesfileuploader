﻿using DelegatesFileUploader.Models.AviationEdge;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Services
{
    /// <summary>
    /// https://aviation-edge.com/developers/
    /// </summary>
    public class AviationEdgeClient : IAviationEdgeClient
    {
        private readonly HttpClient _httpClient;
        private readonly string _apiKey;
        private readonly ILogger _logger;
        public AviationEdgeClient(HttpClient httpClient
            , IConfiguration configuration
            , ILogger<AviationEdgeClient> logger)
        {
            _httpClient = httpClient;
            _apiKey = configuration.GetValue<string>("AVIATIONEDGE_API_KEY");
            _logger = logger;
        }

        public async Task<IList<FlightTimetable>> GetTimetablesAsync(string airportIATA, FlightDirections flightDirection)
        {

            var message = string.Empty;
            try
            {
                var direction = "arrival";
                if (flightDirection == FlightDirections.Departure)
                    direction = "departure";

                var response = await _httpClient
                    .GetAsync($"/v2/public/timetable?key={_apiKey}&iataCode={airportIATA}&type={direction}");
                message = await response.Content.ReadAsStringAsync();
                if(message.Contains("error"))
                {
                    _logger.LogWarning("Error al consultar los vuelos. Response: {Response}", message);
                    return null;
                }
                    
                var rv = JsonConvert.DeserializeObject<List<FlightTimetable>>(message);
                return rv;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Deserialization error. Response: {Response}", message);
                return null;
            }

        }
    }
}
