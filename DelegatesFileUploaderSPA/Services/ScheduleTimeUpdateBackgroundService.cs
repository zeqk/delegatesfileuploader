﻿using DelegatesFileUploader.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Services
{
    public class ScheduleTimeUpdateBackgroundService : BackgroundService, IHostedService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger _logger;

        public ScheduleTimeUpdateBackgroundService(IServiceScopeFactory serviceScopeFactory
            , ILogger<ScheduleTimeUpdateBackgroundService> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var dates = new List<DateTime>();

            var fromDate = DateTime.Today;
            var toDate = new DateTime(2019, 12, 31);

            

            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var updater = scope.ServiceProvider.GetRequiredService<IFlightUpdater>();

                var context = scope.ServiceProvider.GetRequiredService<DelegatesContext>();

                int executionCount = 0;

                while (!stoppingToken.IsCancellationRequested)
                {

                    executionCount++;

                    _logger.LogInformation(
                        "{Date} ScheduleTimeUpdateBackgroundService is working. Count: {Count}", DateTime.Now, executionCount);

                    for (var initialDate = fromDate; initialDate <= toDate; initialDate = initialDate.AddDays(1))
                    {
                        _logger.LogInformation("Actualizando hora de vuelos del {Date}", initialDate.ToString("dd-MM-yyyy"));
                        await updater.UpdateFlightsAsync(initialDate, true);
                    }


                    //await context.UpdateEstimatedDateTimeAsync(DateTime.Today.AddDays(-1).AddMinutes(-1));

                    await Task.Delay(TimeSpan.FromDays(1), stoppingToken);
                }

                    
            }
        }

    }
}
