﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Services.Caspio
{
    public class AuthenticationDelegatingHandler : DelegatingHandler
    {

        private readonly IAuth _auth;

        public AuthenticationDelegatingHandler(IAuth auth)
        {
            _auth = auth;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var token = await _auth.GetTokenAsync(cancellationToken);

            request.Headers.Authorization = new AuthenticationHeaderValue(token.TokenType, token.AccessToken);
            var response = await base.SendAsync(request, cancellationToken);

            if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
            {
                token = await _auth.RefreshTokenAsync(token.RefreshToken, cancellationToken);

                request.Headers.Authorization = new AuthenticationHeaderValue(token.TokenType, token.AccessToken);
                response = await base.SendAsync(request, cancellationToken);
            }

            return response;
        }

        
    }
}
