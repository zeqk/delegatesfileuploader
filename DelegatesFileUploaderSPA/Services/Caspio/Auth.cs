﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Services.Caspio
{
    public class Auth : IAuth
    {

        private static SemaphoreSlim sem = new SemaphoreSlim(1);

        private readonly IConfiguration _configuration;
        private readonly string _baseUrl;
        private readonly string _clientId;
        private readonly string _clientSecret;

        private CaspioToken _caspioToken = null;
        public Auth(IConfiguration configuration)
        {
            _configuration = configuration;
            _baseUrl = _configuration.GetValue<string>("CASPIO_URL") + "/oauth/token";
            _clientId = _configuration.GetValue<string>("CASPIO_CLIENT_ID");
            _clientSecret = _configuration.GetValue<string>("CASPIO_CLIENT_SECRET");
        }

        public async Task<CaspioToken> RefreshTokenAsync(string refereshToken, CancellationToken cancellationToken)
        {
            try
            {
                await sem.WaitAsync(cancellationToken);

                var token = $"{_clientId}:{_clientSecret}";
                var bytes = Encoding.UTF8.GetBytes(token);
                token = Convert.ToBase64String(bytes);

                using (var httpClient = new HttpClient())
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("POST"), _baseUrl))
                    {
                        request.Headers.TryAddWithoutValidation("Authorization", $"Basic {token}");
                        request.Content = new StringContent($"grant_type=refresh_token&refresh_token={refereshToken}");
                        request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                        var response = await httpClient.SendAsync(request, cancellationToken);
                        var json = await response.Content.ReadAsStringAsync();

                        _caspioToken = JsonConvert.DeserializeObject<CaspioToken>(json, new JsonSerializerSettings
                        {

                            ContractResolver = new DefaultContractResolver
                            {
                                NamingStrategy = new SnakeCaseNamingStrategy()
                            }
                        });
                        return _caspioToken;
                    }
                }
            }
            finally
            {
                sem.Release();
            }
        }

        public async Task<CaspioToken> GetTokenAsync(CancellationToken cancellationToken)
        {
            try
            {
                await sem.WaitAsync(cancellationToken);
                if (_caspioToken != null)
                    return _caspioToken;

                var token = $"{_clientId}:{_clientSecret}";
                var bytes = Encoding.UTF8.GetBytes(token);
                token = Convert.ToBase64String(bytes);

                using (var httpClient = new HttpClient())
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("POST"), _baseUrl))
                    {
                        request.Headers.TryAddWithoutValidation("Authorization", $"Basic {token}");


                        request.Content = new StringContent("grant_type=client_credentials");
                        request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                        var response = await httpClient.SendAsync(request, cancellationToken);
                        var json = await response.Content.ReadAsStringAsync();
                        _caspioToken = JsonConvert.DeserializeObject<CaspioToken>(json, new JsonSerializerSettings { 

                            ContractResolver = new DefaultContractResolver
                            {
                                NamingStrategy = new SnakeCaseNamingStrategy()
                            }
                        });
                        return _caspioToken;
                    }
                }

            }
            finally
            {
                sem.Release();
            }
        }
    }
}
