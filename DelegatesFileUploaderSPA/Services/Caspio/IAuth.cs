﻿using System.Threading;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Services.Caspio
{
    public interface IAuth
    {
        Task<CaspioToken> GetTokenAsync(CancellationToken cancellationToken);
        Task<CaspioToken> RefreshTokenAsync(string refereshToken, CancellationToken cancellationToken);
    }
}