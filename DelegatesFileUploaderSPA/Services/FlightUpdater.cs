﻿using DelegatesFileUploader.Convertions;
using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader.Models.AviationEdge;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Services
{
    public class FlightUpdater : IFlightUpdater
    {
        private readonly ILogger _logger;
        private readonly IAviationEdgeClient _aviationEdgeClient;
        private readonly DelegatesContext _delegatesContext;

        public FlightUpdater(IAviationEdgeClient aviationEdgeClient
            , DelegatesContext delegatesContext
            , ILogger<FlightUpdater> logger)
        {
            //hub.CaptureEvent(new SentryEvent() { })
            _aviationEdgeClient = aviationEdgeClient;
            _delegatesContext = delegatesContext;
            _logger = logger;
        }

        public async Task UpdateFlightsAsync(DateTime initialDate, bool updateScheduleTimeMode = false)
        {
            try
            {
                var laterDay = initialDate.AddDays(1);
                var previousDay = initialDate.AddDays(-1);
                var direction = FlightDirections.Arrival;
                if (initialDate > new DateTime(2019, 12, 14))
                    direction = FlightDirections.Departure;

                //Busco los vuelos que todavia no hayan despegado
                var fligths = _delegatesContext.Fligths
                    .Where(f => f.FlightDateTime.Date >= previousDay && f.FlightDateTime.Date < laterDay);

                
                if (direction == FlightDirections.Arrival)
                    fligths = fligths.Where(f => f.FlightStatus != FlightStatuses.Arribado);
                else
                    fligths = fligths.Where(f => f.FlightStatus != FlightStatuses.EnVuelo);

                var fligthsCount = await fligths.CountAsync();
                if (fligthsCount > 0)
                {
                    _logger.LogInformation("Getting timetables for {FlightCount}", fligthsCount);

                    var aux = await fligths.ToListAsync();
                    var airports = aux.GroupBy(f => f.Airport).ToList();


                    var timetablesByAirports = new Dictionary<string, IList<FlightTimetable>>();
                    foreach (var airport in airports)
                    {
                        var iataCode = Utils.GetIATACode(airport.Key);

                        var timetable = await _aviationEdgeClient.GetTimetablesAsync(iataCode, direction);
                        
                        timetablesByAirports.Add(iataCode, timetable);
                    }


                    foreach (var airport in airports)
                    {
                        var iataCode = Utils.GetIATACode(airport.Key);
                        var timetables = timetablesByAirports[iataCode];

                        if (timetables != null)
                        {

                            foreach (var flight in airport)
                            {
                                var day = flight.FlightDateTime.Date == initialDate.Date ? DateTime.Today :
                                    (flight.FlightDateTime.Date == previousDay.Date ? DateTime.Today.AddDays(-1) : DateTime.Today.AddDays(1));

                                //Normalizacion del numero de vuelo
                                
                                var normalizedFlightNumber = flight.NormalizedFlightNumber;
                                
                                if(!Regex.IsMatch(normalizedFlightNumber, @"^([A-Z]{2}|[A-Z]\d|\d[A-Z])[1-9](\d{1,3})?$"))
                                {
                                    if(normalizedFlightNumber.StartsWith("*1") && flight.Airline.Contains("Lufthansa"))
                                    {
                                        normalizedFlightNumber = normalizedFlightNumber.Replace("*1", "LH");                                        
                                    }
                                    else if (normalizedFlightNumber.StartsWith("*2") && flight.Airline.Contains("Oneworld"))
                                    {
                                        //ej. *22428
                                        normalizedFlightNumber = normalizedFlightNumber.Replace("*2", "LA");
                                    }
                                    else
                                    {
                                        var secondPart = normalizedFlightNumber.Substring(2);
                                        var validSecondPart = secondPart.TrimStart('0');
                                        normalizedFlightNumber = normalizedFlightNumber.Replace(secondPart, validSecondPart);
                                    }
                                }

                                var fsQuery = timetables
                                    .Where(t => t.Flight?.IataNumber == normalizedFlightNumber);

                                if (direction == FlightDirections.Arrival)
                                    fsQuery = fsQuery.Where(t => t.Arrival.ScheduledTime.HasValue && 
                                                                t.Arrival.ScheduledTime.Value.Date.Equals(day));
                                else
                                    fsQuery = fsQuery.Where(t => t.Departure.ScheduledTime.HasValue && 
                                                                t.Departure.ScheduledTime.Value.Date.Equals(day));

                                var timetable = fsQuery.FirstOrDefault();

                                if(timetable != null) 
                                {
                                    var scheduleDateTime = direction == FlightDirections.Arrival ?
                                        timetable.Arrival.ScheduledTime.Value : timetable.Departure.ScheduledTime.Value;

                                    if (scheduleDateTime.Hour != flight.FlightDateTime.Hour || scheduleDateTime.Minute != flight.FlightDateTime.Minute)
                                    {
                                        var message = $"Se actualiza el horario del vuelo {flight.FlightNumber} ({flight.FlightDateTime.ToString("dd-MM-yyyy")}). " +
                                            $"Hora provista por el delegado {flight.FlightDateTime.ToString("HH:mm")}. " +
                                            $"Hora programada por la aerolínea {scheduleDateTime.ToString("HH:mm")}";
                                        _logger.LogInformation(message);
                                    }
                                                                        
                                    flight.FlightDateTime = new DateTime(flight.FlightDateTime.Year, flight.FlightDateTime.Month,
                                        flight.FlightDateTime.Day, scheduleDateTime.Hour, scheduleDateTime.Minute, scheduleDateTime.Second);
                                    flight.FlightHour = scheduleDateTime.Hour;

                                    if (!updateScheduleTimeMode)
                                    {

                                        flight.Status = timetable.Status;
                                        flight.FlightStatus = Utils.GetFlightStatuses(timetable.Status);

                                        flight.EstimatedDateTime = direction == FlightDirections.Arrival ?
                                            timetable?.Arrival.EstimatedTime : timetable?.Departure.EstimatedTime;

                                        flight.ActualDateTime = direction == FlightDirections.Arrival ?
                                            timetable?.Arrival.ActualTime : timetable?.Departure.ActualTime;

                                        flight.EstimatedRunway = direction == FlightDirections.Arrival ?
                                            timetable?.Arrival.EstimatedRunway : timetable?.Departure.EstimatedRunway;

                                        flight.ActualRunway = direction == FlightDirections.Arrival ?
                                            timetable?.Arrival.ActualRunway : timetable?.Departure.ActualRunway;

                                        //Aveces nunca se obtiene el estado Arribado, asi q se setea manualmente si ya paso la hora estimada de llegada
                                        //Si la hora de arribo estimada es menor que la hora actual
                                        if (direction == FlightDirections.Arrival && flight.EstimatedDateTime < DateTime.Now)
                                        {
                                            flight.Status = "landed";
                                            flight.FlightStatus = FlightStatuses.Arribado;
                                        }
                                    }

                                    flight.FlightURL = $"https://www.flightradar24.com/data/flights/{normalizedFlightNumber}";
                                    flight.NormalizedFlightNumber = normalizedFlightNumber;

                                    flight.UpdateStatusDateTime = DateTime.Now;

                                    await _delegatesContext.SaveChangesAsync();
                                }
                            }
                        }
                    }

                    //Actualizo el tiempo de demora cada vez
                    await _delegatesContext.UpdateFligthsDelayedMinutesAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}
