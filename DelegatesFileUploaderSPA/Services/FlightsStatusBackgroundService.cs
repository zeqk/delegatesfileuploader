﻿using DelegatesFileUploader.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Services
{
    public class FlightsStatusBackgroundService : BackgroundService, IHostedService
    {
        public IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger _logger;
        private int _minutes = 0;

        public FlightsStatusBackgroundService(IServiceScopeFactory serviceScopeFactory
            , ILogger<FlightsStatusBackgroundService> logger
            , IConfiguration configuration)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
            _minutes = configuration.GetValue<int>("REFRESH_FLIGHTS_MINS");
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var updater = scope.ServiceProvider.GetRequiredService<IFlightUpdater>();
                var context = scope.ServiceProvider.GetRequiredService<DelegatesContext>();

                int executionCount = 0;

                while (!stoppingToken.IsCancellationRequested)
                {
                    executionCount++;

                    _logger.LogInformation("{Date} FlightsStatusBackgroundService is working. Count: {Count}", DateTime.Now, executionCount);

                    await updater.UpdateFlightsAsync(DateTime.Today);


                    //Aveces nunca se obtiene el estado Arribado, asi q se setea manualmente si ya paso la hora estimada de llegada
                    //var count = await context.SetLandedFlightStatusAsync(DateTime.Now);

                    //_logger.LogInformation("{Date} FlightsStatusBackgroundService se actualizaron {Count} vuelos como arribados", DateTime.Now, count);

                    await Task.Delay(TimeSpan.FromMinutes(_minutes), stoppingToken);
                }

                    
            }
        }

    }
}
