﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace DelegatesFileUploaderSPA.Services
{



    public interface IPassengerListService
    {
        Task<byte[]> GetPassengerListFilesAsync(PSFormData data);
        Task<IList<PlusmarForm>> GetPassengerListAsync(PSFormData data);
    }


    public class PSFormData
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string CUIT { get; set; }
        [Required]
        public string Address { get; set; }

        [Required]
        public DateTime DateFrom { get; set; }

        [Required]
        public DateTime DateTo { get; set; }
    }

    public class Passenger
    {

        public string Lastname { get; set; }
        public string Firstname { get; set; }

        public string LegalIdType { get; set; }

        public string LegalIdNumber { get; set; }

        public string UnderAge { get; set; }

        public string Nationality { get; set; }

        public string Gender { get; set; }
    }

    public class PlusmarForm
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CUIT { get; set; }

        public string Address { get; set; }

        public string Details { get; set; }

        public IList<Passenger> Passengers { get; set; }
    }
}