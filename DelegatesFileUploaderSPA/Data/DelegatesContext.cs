﻿using DelegatesFileUploader.Models;
using DelegatesFileUploaderSPA.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Delegate = DelegatesFileUploader.Models.Delegate;

namespace DelegatesFileUploader.Data
{
    public class DelegatesContext : DbContext
    {
        public DelegatesContext()
            : base()
        { }

        public DelegatesContext(DbContextOptions<DelegatesContext> options)
            : base(options)
        { }

        public DbSet<Delegate> Delegates { get; set; }

        public DbSet<DelegatesFileUploader.Models.Activity> Activities { get; set; }

        public DbSet<MainActivity> MainActivities { get; set; }

        public DbSet<Accommodation> Accommodations { get; set; }

        public DbSet<Flight> Fligths { get; set; }

        public DbSet<Volunteer> Volunteers { get; set; }

        public DbSet<VolunteerAssignment> VolunteerAssignments { get; set; }

        public DbSet<Importation> Importations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Delegate>(entity =>
            {
                entity.HasKey(e => e.ApplicantNo)
                        .HasAnnotation("MySQL:AutoIncrement", false);

                entity.HasIndex(d => d.ApplicantGroupNo);
                entity.HasIndex(d => d.ApplicantNo);                
                entity.HasIndex(d => d.ArrivalAirline);
                entity.HasIndex(d => d.ArrivalAirport);
                entity.HasIndex(d => d.DepartureAirline);
                entity.HasIndex(d => d.DepartureAirport);
                entity.HasIndex(d => d.HotelReference);
            });

            modelBuilder.Entity<Activity>(entity =>
            {
                entity.HasKey(e => e.Id);
                //entity.HasIndex(e => e.ActivityName);
                entity.HasIndex(e => e.ApplicantNo);
                entity.HasIndex(e => e.Hotel);
            });

            modelBuilder.Entity<Accommodation>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasIndex(e => e.Name);
                entity.Property(e => e.Type).HasConversion<string>();
                
            });

            modelBuilder.Entity<Flight>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasIndex(e => e.Airline);
                entity.HasIndex(e => e.Airport);
                entity.HasIndex(e => e.FlightHour);
                entity.HasIndex(e => e.FlightNumber);
                entity.HasIndex(e => e.NormalizedFlightNumber);
                entity.Property(e => e.FlightStatus).HasConversion<string>();

            });



            modelBuilder.Entity<Importation>().Property(d => d.Type).HasConversion<string>();
        }

        /// <summary>
        /// Elimina todos los delegados
        /// </summary>
        /// <returns></returns>
        public async Task<int> DeleteAllDelegatesAsync()
        {
            return await Database.ExecuteSqlRawAsync("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE Delegates; SET FOREIGN_KEY_CHECKS = 1;");
        }

        /// <summary>
        /// Elimina todas las actividades
        /// </summary>
        /// <returns></returns>
        public async Task<int> DeleteAllActivitiesAsync()
        {
            return await Database.ExecuteSqlRawAsync("TRUNCATE TABLE Activities");
        }

        /// <summary>
        /// Elimina todas las hotels
        /// </summary>
        /// <returns></returns>
        public async Task<int> DeleteAllAccommodationsAsync()
        {
            return await Database.ExecuteSqlRawAsync("SET FOREIGN_KEY_CHECKS = 0; " +
               "TRUNCATE TABLE Accommodations; " +
               "SET FOREIGN_KEY_CHECKS = 1; ");
        }

        /// <summary>
        /// Elimina todos los vuelos
        /// </summary>
        /// <returns></returns>
        public async Task<int> DeleteAllFligthsAsync()
        {
            return await Database.ExecuteSqlRawAsync("SET FOREIGN_KEY_CHECKS = 0; " +
                "TRUNCATE TABLE Fligths; " +
                "SET FOREIGN_KEY_CHECKS = 1; ");
        }

        /// <summary>
        /// Elimina todos los vuelos
        /// </summary>
        /// <returns></returns>
        public async Task<int> UpdateFligthsDelayedMinutesAsync()
        {
            return await Database.ExecuteSqlRawAsync($"UPDATE Fligths " +
                $"SET DelayedMinutes = TIMESTAMPDIFF(MINUTE,FlightDateTime,EstimatedDateTime) " +
                $"WHERE EstimatedDateTime IS NOT NULL");
        }

        public async Task<int> UpdateEstimatedDateTimeAsync(DateTime dateTime)
        {
            
            var FlightDateTime = new MySql.Data.MySqlClient.MySqlParameter("@FlightDateTime", dateTime);

            return await Database.ExecuteSqlRawAsync("UPDATE Fligths SET EstimatedDateTime = FlightDateTime " +
                "WHERE FlightDateTime > @FlightDateTime", FlightDateTime);
        }

        public async Task<int> SetLandedFlightStatusAsync(DateTime dateTime)
        {
            var FlightDateTime = new MySql.Data.MySqlClient.MySqlParameter("@EstimatedDateTime", dateTime);

            return await Database.ExecuteSqlRawAsync("UPDATE Fligths SET Status = 'landed', FlightStatus = 'Arribado' " +
                "WHERE EstimatedDateTime < @EstimatedDateTime", FlightDateTime);
        }


    }



}
