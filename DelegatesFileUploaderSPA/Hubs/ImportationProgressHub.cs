﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace DelegatesFileUploader2.Hubs
{
    public class ImportationProgressHub : Hub
    {
        public async Task AssociateJob(string jobId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, jobId);
        }
    }

}
