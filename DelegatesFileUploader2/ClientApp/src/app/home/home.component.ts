import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpEvent, HttpEventType, HttpClient } from '@angular/common/http';
import { FileUploadService } from '../file-upload.service';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

    form: FormGroup;
    progress: number = 0;
    progress2: number = 0;
    inProgress = false;

    constructor(
        public fb: FormBuilder,
        public fileUploadService: FileUploadService,
        private http: HttpClient,
        @Inject('BASE_URL') private baseUrl: string
    ) {
        this.form = this.fb.group({
            name: [''],
            avatar: [null]
        })
    }

    ngOnInit() { }

    uploadFile(event) {
        const file = (event.target as HTMLInputElement).files[0];
        this.form.patchValue({
            avatar: file
        });
        this.form.get('avatar').updateValueAndValidity()
    }

    submitUser() {

        this.progress2 = 0;
        this.inProgress = true;

        let hubConnection = new HubConnectionBuilder()
            .withUrl(this.baseUrl + 'jobprogress')
            .build();

        hubConnection
            .start()
            .then(() => {
                hubConnection.invoke('AssociateJob', "hola");
            })
            .catch(err => {
                console.log(err);
            });

        hubConnection.on('progress', (data: number) => {
            this.progress2 = data;
        });

        this.fileUploadService.addUser(
            this.form.value.name,
            this.form.value.avatar
        ).subscribe((event: HttpEvent<any>) => {
            switch (event.type) {
                case HttpEventType.Sent:
                    console.log('Request has been made!');
                    break;
                case HttpEventType.ResponseHeader:
                    console.log('Response header has been received!');
                    break;
                case HttpEventType.UploadProgress:

                    this.progress = Math.round(event.loaded / event.total * 100);
                    console.log(`Uploaded! ${this.progress}%`);
                    break;
                case HttpEventType.Response:
                    console.log('User successfully created!', event.body);
                    hubConnection.stop();
                    this.inProgress = false;
                    setTimeout(() => {
                        this.progress = 0;
                    }, 1500);

            }
        });
    }


}
