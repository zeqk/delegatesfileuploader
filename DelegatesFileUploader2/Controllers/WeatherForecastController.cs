﻿using DelegatesFileUploader2.Hubs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploader2.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IHubContext<JobProgressHub> _hubContext;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IHubContext<JobProgressHub> hub)
        {
            _logger = logger;
            _hubContext = hub;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost,AllowAnonymous]
        public async Task Upload(IFormFile avatar)
        {

            for (int i = 1; i <= 10; i++)
            {
                await _hubContext.Clients.Group("import").SendAsync("progress", 10 * i);
                await Task.Delay(3000);
            }

            _logger.LogDebug("Subido");
        }
    }
}
