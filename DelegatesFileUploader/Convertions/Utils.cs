﻿using DelegatesFileUploader.Models;

namespace DelegatesFileUploader.Convertions
{
    public static class Utils
    {
        public static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public static string GetIATACode(string airport)
        {
            var rv = airport.Split("-")[0].Trim();
            return rv;
        }

        public static FlightStatuses GetFlightStatuses(string value)
        {
            switch (value)
            {
                case "landed":
                    return FlightStatuses.Arribado;
                case "scheduled":
                    return FlightStatuses.Programado;
                case "cancelled":
                    return FlightStatuses.Cancelado;
                case "active":
                    return FlightStatuses.Despegado;
                case "incident":
                    return FlightStatuses.Incidente;
                case "diverted":
                    return FlightStatuses.Desviado;
                case "redirected":
                    return FlightStatuses.Redirigido;
                default:
                    return FlightStatuses.Desconocido;
            }
        }
    }
}
