﻿using CsvHelper.Configuration;
using DelegatesFileUploader.Models;
using System;
using System.Globalization;

namespace DelegatesFileUploader.Convertions
{
    public class ActivityMap : ClassMap<Activity>
    {
        public ActivityMap()
        {
            Map(m => m.ActivityName);
            Map(m => m.ApplicantNo);
            Map(m => m.Hotel);
            Map(m => m.SlotName);
            Map(m => m.Start);
            Map(m => m.StartDateTime).Ignore(true).ConvertUsing(row =>
            {
                return DateTime.ParseExact(row.GetField("Start"), "MMM dd yyyy h:mmtt", CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces);
            });
            Map(m => m.StartHour).Ignore(true).ConvertUsing(row =>
            {
                return DateTime.ParseExact(row.GetField("Start"), "MMM dd yyyy h:mmtt", CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces).Hour;
            });
        }
    }
}
