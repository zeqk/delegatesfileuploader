﻿using DelegatesFileUploader.Models;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;

namespace DelegatesFileUploader.Convertions
{
    public static class IRowExtensions
    {
        public static SpecialRooming GetSpecialRooming(this IRow row)
        {
            var name = row.GetValue(0, v => v);

            if (name == null)
                return null;

            var rv = new SpecialRooming();

            rv.DelegateName = name;            
            rv.ApplicantNo = row.GetValue(1, v => Convert.ToInt64(v));
            rv.GroupNo = row.GetValue(2, v => Convert.ToInt64(v));
            rv.BranchName = row.GetValue(3, v => v);
            rv.AccommodationName = row.GetValue(4, v => v);
            rv.AccomodationAddress = row.GetValue(5, v => v);
            rv.AccommodationCoordinates = row.GetValue(6, v => v);

            return rv;
        }

        public static Models.Delegate GetDelegate(this IRow row, IDictionary<string, int> ic)
        {
            var d = new Models.Delegate();
            d.ApplicantNo = row.GetValue(ic, "Applicant No", v => Convert.ToInt64(v));
            try
            {
                d.Activity = row.GetValue(ic, "Activity");
                d.ApplicantGroupNo = row.GetValue(ic, "Applicant Group No", v => Convert.ToInt64(v));
                d.ApplicantStatus = row.GetValue(ic, "Applicant Status");
                d.ArrivalAirline = row.GetValue(ic, "Arrival Airline");
                d.ArrivalAirport = row.GetValue(ic, "Arrival Airport");
                d.ArrivalDay = row.GetValue(ic, "Arrival Day");
                d.ArrivalFlight = row.GetValue(ic, "Arrival Flight");
                d.ArrivalDateTime = row.GetDateTimeValue(ic, "Arrival Time", "dd/MM/yyyy H:mm");
                d.ArrivalTransport = row.GetValue(ic, "Arrival Transport");
                d.AvailableDays = row.GetValue(ic, "Available Days", v => Convert.ToInt32(v), 0);
                d.AvailableFrom = row.GetValue(ic, "Available From");
                d.AvailableFromDate = row.GetDateTimeValue(ic, "Available From", "dd/MM/yyyy");
                d.AvailableTo = row.GetValue(ic, "Available To");
                d.AvailableToDate = row.GetDateTimeValue(ic, "Available To", "dd/MM/yyyy");
                d.BranchName = row.GetValue(ic, "Branch Name");
                d.CheckInDateTime = row.GetDateTimeValue(ic, "Check In", "dd/MM/yyyy");
                d.CheckOutDateTime = row.GetDateTimeValue(ic, "Check Out", "dd/MM/yyyy");
                d.ConfirmedDate = row.GetDateTimeValue(ic, "Confirmed Date", "dd/MM/yyyy");

                d.DepartureAirline = row.GetValue(ic, "Departure Airline");
                d.DepartureAirport = row.GetValue(ic, "Departure Airport");
                d.DepartureDay = row.GetValue(ic, "Departure Day");
                d.DepartureFlight = row.GetValue(ic, "Departure Flight");
                d.DepartureDateTime = row.GetDateTimeValue(ic, "Departure Time", "dd/MM/yyyy H:mm");
                d.DepartureTransport = row.GetValue(ic, "Departure Transport");

                d.Dietary = row.GetValue(ic, "Dietary");
                d.EdiStatus = row.GetValue(ic, "Edi Status");
                d.ForeignServant = row.GetValue(ic, "Foreign Servant", v => v == "Yes");
                d.FromRestrictedCountry = row.GetValue(ic, "From Restricted Country", v => v == "Yes");
                d.Gender = row.GetValue(ic, "Gender");

                d.GroupArrival = row.GetValue(ic, "Group Arrival");
                d.GroupArrivalDateTime = row.GetDateTimeValue(ic, "Group Arrival", "dd/MM/yyyy H:mm");
                d.GroupDeparture = row.GetValue(ic, "Group Departure");
                d.GroupDepartureDateTime = row.GetDateTimeValue(ic, "Group Departure", "dd/MM/yyyy H:mm");

                d.HotelReference = row.GetValue(ic, "Hotel Name");

                d.IndividualArrival = row.GetValue(ic, "Individual Arrival");
                d.IndividualArrivalDateTime = row.GetDateTimeValue(ic, "Individual Arrival", "dd/MM/yyyy H:mm");
                d.IndividualDeparture = row.GetValue(ic, "Individual Departure");
                d.IndividualDepartureDateTime = row.GetDateTimeValue(ic, "Individual Departure", "dd/MM/yyyy H:mm");

                d.IsGroupLeader = row.GetValue(ic, "Is Group Leader", v => v == "Yes");
                d.IsSpecialGuest = row.GetValue(ic, "Is Special Guest", v => v == "Yes");
                d.Language = row.GetValue(ic, "Language");
                d.LastChangeDate = row.GetDateTimeValue(ic, "Last Change", "dd/MM/yyyy").Value;
                d.Name = row.GetValue(ic, "Name");
                d.Schedule = row.GetValue(ic, "Schedule", v => (int?)Convert.ToInt32(v));
                d.SelectedDate = row.GetDateTimeValue(ic, "Selected Date", "dd/MM/yyyy");
                d.SFTS = row.GetValue(ic, "SFTS", v => v == "Yes");
                d.SpecialRooming = row.GetValue(ic, "Special Rooming", v => v == "Yes");

                return d;
            }
            catch (Exception ex)
            {
                throw new Exception($"Applicant No {d.ApplicantNo} " + ex.Message);
            }            
        }

        public static string GetValue(this IRow row, IDictionary<string, int> indexByColumnName, string columnName)
        {
            Func<string, string> convertion = v => v;
            return GetValue(row, indexByColumnName, columnName, convertion);
        }

        public static T GetValue<T>(this IRow row, IDictionary<string, int> indexByColumnName, string columnName, Func<string, T> convertion, object defaultValue = null)
        {
            try
            {
                if (!indexByColumnName.ContainsKey(columnName))
                    return (T)defaultValue;

                var value = row.Cells[indexByColumnName[columnName]].StringCellValue;


                if (String.IsNullOrEmpty(value))
                    return (T)defaultValue;

                var rv = convertion(value);

                return rv;

            }
            catch (Exception ex)
            {
                throw new Exception($"Convertion error Field {columnName} " + ex.Message);
            }
        }

        public static T GetValue<T>(this IRow row, int index, Func<string, T> convertion, object defaultValue = null)
        {
            try
            {
                var value = (string)null;
                if (index < row.Cells.Count)
                    value = row.Cells[index].StringCellValue;                

                if (String.IsNullOrEmpty(value))
                    return (T)defaultValue;

                var rv = convertion(value);

                return rv;

            }
            catch (Exception ex)
            {
                throw new Exception($"Convertion error Field Index {index} " + ex.Message);
            }
        }

        public static DateTime? GetDateTimeValue(this IRow row, IDictionary<string, int> indexByColumnName, string columnName, string format, DateTime? defaultValue = null)
        {
            try
            {
                if (!indexByColumnName.ContainsKey(columnName))
                    return defaultValue;

                string value = null;
                try
                {
                    value = row.Cells[indexByColumnName[columnName]].StringCellValue;
                }
                catch (Exception ex)
                {
                    var dateValue = row.Cells[indexByColumnName[columnName]].DateCellValue;
                    return dateValue;

                }


                if (String.IsNullOrEmpty(value))
                    return defaultValue;

                var rv = (DateTime?)DateTime.ParseExact(value, format, null);

                return rv;

            }
            catch (Exception ex)
            {
                throw new Exception($"Convertion error Field {columnName} " + ex.Message);
            }
        }
    }
}
