﻿using CsvHelper;
using DelegatesFileUploader.Convertions;
using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Controllers
{
    public class ActivitiesController : Controller
    {
        private readonly DelegatesContext _context;
        private readonly IConfiguration _configuration;

        public ActivitiesController(DelegatesContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        
        public IActionResult Index()
        {
            var result = new DelegatesViewModel
            {
                LastChangeDate = _context.Importations.Where(i => i.Type == ImportationTypes.Activities).DefaultIfEmpty().Max(i => i.DateTime)
            };

            return View(result);
        }



        [HttpPost]
        public async Task<IActionResult> Index([Required]IFormFile file, [Required]string password)
        {

            if (password != _configuration.GetValue<string>("UploadPassword"))
                return Unauthorized("No autorizado");

            var result = new DelegatesViewModel();

            try
            {
                IEnumerable<Activity> activities = null;

                using (var stream = file.OpenReadStream())
                using (var reader = new StreamReader(stream))
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.Delimiter = ";";
                    csv.Configuration.RegisterClassMap<ActivityMap>();
                    activities = csv.GetRecords<Activity>().ToList();
                }

                var deletedActivitiesCount = await _context.DeleteAllActivitiesAsync();
                await _context.Activities.AddRangeAsync(activities);



                var importation = new Importation
                {
                    DateTime = DateTime.Now,
                    FileName = file.FileName,
                    RowCount = activities.Count(),
                    SizeMB = Utils.ConvertBytesToMegabytes(file.Length),
                    Type = ImportationTypes.Activities
                };

                await _context.Importations.AddAsync(importation);

                await _context.SaveChangesAsync();

                result.SuccessfulMessage = $"Se agregaron {activities.Count()} registros de actividades.";

            }
            catch (Exception ex)
            {
                result.ErrorMessage = ex.Message + ex.InnerException?.Message;
            }


            result.Init = false;
            return View(result);
        }

    }
}
