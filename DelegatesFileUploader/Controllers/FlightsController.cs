﻿using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class FlightsController : Controller
    {
        private readonly DelegatesContext _context;
        private readonly IConfiguration _configuration;
        private readonly IFlightUpdater _flightStatusUpdater;

        public FlightsController(DelegatesContext context,
            IFlightUpdater flightStatusUpdater,
            IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            _flightStatusUpdater = flightStatusUpdater;
        }

        public IActionResult Index()
        {
            var result = new FlightsViewModel
            {
                InitialDate = DateTime.Today
            };

            return View(result);
        }


        [HttpPost("update")]
        public async Task<IActionResult> UpdateFligthsAsync([Required,FromBody]FlightsViewModel model)
        {
            if (model.Password != _configuration.GetValue<string>("UploadPassword"))
                return Unauthorized("No autorizado");

            await _flightStatusUpdater.UpdateFlightsAsync(model.InitialDate);

            return NoContent();
        }

    }
}
