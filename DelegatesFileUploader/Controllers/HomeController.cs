﻿using DelegatesFileUploader.Convertions;
using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger _logger;
        private readonly DelegatesContext _delegatesContext;
        private readonly IConfiguration _configuration;
        private readonly IGeocoding _geocoding;

        public HomeController(ILogger<HomeController> logger, DelegatesContext delegatesContext, 
            IConfiguration configuration,
            IGeocoding geocoding)
        {
            _logger = logger;
            _delegatesContext = delegatesContext;
            _configuration = configuration;
            _geocoding = geocoding;
        }

        public async Task<IActionResult> Index()
        {
            var result = new DelegatesViewModel
            {
                LastChangeDate = await _delegatesContext.Importations
                            .Where(i => i.Type == ImportationTypes.Delegates)
                            .DefaultIfEmpty().MaxAsync(i => i.DateTime)
            };
            
            return View(result);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        #region upload
        [HttpPost]
        public async Task<IActionResult> Index([Required]IFormFile file, [Required]string password)
        {
            
            if (password != _configuration.GetValue<string>("UploadPassword"))
                return Unauthorized("No autorizado");

            ISheet sheet;
            using (var stream = file.OpenReadStream())
            {

                var wb = WorkbookFactory.Create(stream);
                sheet = wb.GetSheetAt(0);
            }

            var rv = new DelegatesViewModel();
            try
            {
                var delegates = new List<Models.Delegate>();

                var headerRow = sheet.GetRow(0);
                var ic = headerRow.Cells.ToDictionary(c => c.StringCellValue, c => c.ColumnIndex);

                _logger.LogDebug("PhysicalNumberOfRows {PhysicalNumberOfRows}", sheet.PhysicalNumberOfRows);


                var currentFlights = await _delegatesContext.Fligths.ToListAsync();
                
                var updateDateTime = DateTime.Now;
                var arrivalFligths = currentFlights.Where(f => !f.Departure).ToDictionary(f => f.FlightDateTime.ToString("yyyyMMdd") + "-" + f.FlightNumber.Trim());
                var departureFligths = currentFlights.Where(f => f.Departure).ToDictionary(f => f.FlightDateTime.ToString("yyyyMMdd") + "-" + f.FlightNumber.Trim());

                var accomodations = new Dictionary<string, Accommodation>();

                var insertedFlightsCount = 0;
                var insertedAccomodationsCount = 0;

                for (int i = 1; i < sheet.PhysicalNumberOfRows; i++)
                {
                    var row = sheet.GetRow(i);
                    var d = row.GetDelegate(ic);
                    try
                    {

                        d.ArrivalHour = d.ArrivalDateTime?.Hour;
                        d.ArrivalSetted = d.ArrivalDateTime.HasValue;
                        d.ArrivalSettedText = d.ArrivalSetted ? "Si" : "No";
                        d.CheckInHour = d.CheckInDateTime?.Hour;
                        d.CheckOutHour = d.CheckOutDateTime?.Hour;
                        d.DepartureHour = d.DepartureDateTime?.Hour;
                        d.DepartureSetted = d.DepartureDateTime.HasValue;
                        d.DepartureSettedText = d.DepartureSetted ? "Si" : "No";
                        d.ArrivalAndCheckingSameDay = (d.CheckInDateTime - d.ArrivalDateTime)?.TotalDays <= 1;
                        d.DepartureAndCheckoutSameDay = (d.DepartureDateTime - d.CheckOutDateTime)?.TotalDays <= 1;


                        //vuelos
                        if (d.ApplicantStatus == "Confirmed"
                            && !String.IsNullOrEmpty(d.ArrivalFlight)
                            && d.ArrivalDateTime.HasValue)
                        {
                            var arrivalFlightKey =  d.ArrivalDateTime.Value.ToString("yyyyMMdd") + "-" + d.ArrivalFlight.Trim();

                            if (!arrivalFligths.ContainsKey(arrivalFlightKey))
                            {
                                var flight = new Flight
                                {
                                    Airline = d.ArrivalAirline,
                                    Airport = d.ArrivalAirport,
                                    FlightDateTime = d.ArrivalDateTime.Value,
                                    JWFlightDateTime = d.ArrivalDateTime.Value,
                                    FlightHour = d.ArrivalHour.Value,
                                    JWFlightHour = d.ArrivalHour.Value,
                                    FlightNumber = d.ArrivalFlight,
                                    FlightURL = $"https://www.flightradar24.com/data/flights/{d.ArrivalFlight}",
                                    Departure = false,
                                    Status = "scheduled",
                                    FlightStatus = FlightStatuses.Programado
                                };

                                var e = await _delegatesContext.Fligths.AddAsync(flight);
                                arrivalFligths.Add(arrivalFlightKey, e.Entity);
                                insertedFlightsCount++;
                            }

                            d.ArrivalFlightData = arrivalFligths[arrivalFlightKey];

                        }

                        if (d.ApplicantStatus == "Confirmed"
                            && !String.IsNullOrEmpty(d.DepartureFlight) 
                            && d.DepartureDateTime.HasValue)
                        {
                            var departureFlightKey = d.DepartureDateTime.Value.ToString("yyyyMMdd") + "-" + d.DepartureFlight.Trim();

                            if (!departureFligths.ContainsKey(departureFlightKey))
                            {
                                var flight = new Flight
                                {
                                    Airline = d.DepartureAirline,
                                    Airport = d.DepartureAirport,
                                    FlightDateTime = d.DepartureDateTime.Value,
                                    JWFlightDateTime = d.DepartureDateTime.Value,
                                    FlightHour = d.DepartureHour.Value,
                                    JWFlightHour = d.DepartureHour.Value,
                                    FlightNumber = d.DepartureFlight,
                                    FlightURL = $"https://www.flightradar24.com/data/flights/{d.DepartureFlight}",
                                    Departure = true,
                                    Status = "scheduled",
                                    FlightStatus = FlightStatuses.Programado
                                };

                                var e = await _delegatesContext.Fligths.AddAsync(flight);
                                departureFligths.Add(departureFlightKey, e.Entity);
                                insertedFlightsCount++;
                            }

                            d.DepartureFlightData = departureFligths[departureFlightKey];

                        }

                        //Accomodation
                        if (!string.IsNullOrEmpty(d.HotelReference))
                        {
                            if (!accomodations.ContainsKey(d.HotelReference))
                            {
                                var hotel = getAccommodation(d.HotelReference);
                                var entity = await _delegatesContext.Accommodations.AddAsync(hotel);
                                accomodations.Add(d.HotelReference, entity.Entity);
                                insertedAccomodationsCount++;
                            }

                            d.Accommodation = accomodations[d.HotelReference];
                        }

                        delegates.Add(d);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Applicant No {d.ApplicantNo} " + ex.Message);
                    }
                }

                _logger.LogDebug("Delegates {DelegatesCount}", delegates.Count);

                var deletedDelegatesCount = await _delegatesContext.DeleteAllDelegatesAsync();
                var deletedAccomodationsCount = await _delegatesContext.DeleteAllAccommodationsAsync();

                await _delegatesContext.Delegates.AddRangeAsync(delegates);

                rv.SuccessfulMessage = $"Se agregaron {delegates.Count} registros de delegados." +
                    $" Se agregaron {insertedFlightsCount} registros de vuelos." +
                    $" Se agregaron {insertedAccomodationsCount} registros de alojamientos.";
                

                var importation = new Importation
                {
                    DateTime = DateTime.Now,
                    FileName = file.FileName,
                    RowCount = sheet.PhysicalNumberOfRows,
                    SizeMB = Utils.ConvertBytesToMegabytes(file.Length),
                    Type = ImportationTypes.Delegates
                };

                _delegatesContext.Importations.Add(importation);

                await _delegatesContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                rv.ErrorMessage = ex.Message;
            }
            rv.Init = false;
            return View(rv);
        }

        Accommodation getAccommodation(string name)
        {
            try
            {
                var h = new Accommodation
                {
                    Name = name
                };

                //var geo = _geocoding.Geocode(name + ", Buenos Aires, Argentina");
                //h.Latitude = geo.latitude;
                //h.Longitude = geo.longitude;
                return h;
            }
            catch (Exception ex)
            {
                _logger.LogError("{Hotel} geocode error: {Error}", name, ex.Message);
                throw;
            }
        }


        #endregion
    }
}
