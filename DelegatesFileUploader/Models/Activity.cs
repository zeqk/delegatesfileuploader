﻿using System;

namespace DelegatesFileUploader.Models
{
    public class Activity
    {
        public long Id { get; set; }

        public long ApplicantNo { get; set; }

        public string Hotel { get; set; }

        public string ActivityName { get; set; }

        public string Start { get; set; }

        public DateTime StartDateTime { get; set; }

        public int StartHour { get; set; }

        public string SlotName { get; set; }
    }
}
