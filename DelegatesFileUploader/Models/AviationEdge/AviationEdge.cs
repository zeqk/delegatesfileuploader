﻿using System;

namespace DelegatesFileUploader.Models.AviationEdge
{
    public class FlightTimetable
    {
        public string Type { get; set; }
        public string Status { get; set; }
        public Arrival Departure { get; set; }
        public Arrival Arrival { get; set; }
        public Airline Airline { get; set; }
        public Flight Flight { get; set; }

        public CodeShared CodeShared { get; set; }
    }

    public class Airline
    {
        public string Name { get; set; }
        public string IataCode { get; set; }
        public string IcaoCode { get; set; }
    }

    public class Arrival
    {
        public string IataCode { get; set; }
        public string IcaoCode { get; set; }

        public DateTime? ScheduledTime { get; set; }

        public DateTime? EstimatedTime { get; set; }
        public DateTime? ActualTime { get; set; }
        public DateTime? EstimatedRunway { get; set; }
        public DateTime? ActualRunway { get; set; }
        
    }

    public class Flight
    {
        public string Number { get; set; }
        public string IataNumber { get; set; }
        public string IcaoNumber { get; set; }
    }

    public class CodeShared
    {
        public Airline Airline { get; set; }
        public Flight Flight { get; set; }
    }

}
