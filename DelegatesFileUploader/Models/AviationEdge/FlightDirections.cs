﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Models.AviationEdge
{
    public enum FlightDirections
    {
        Departure,
        Arrival
    }
}
