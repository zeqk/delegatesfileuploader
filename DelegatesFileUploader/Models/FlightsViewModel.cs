﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DelegatesFileUploader.Models
{
    public class FlightsViewModel
    {
        
        [Required, Display(Name = "Fecha")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime InitialDate { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
