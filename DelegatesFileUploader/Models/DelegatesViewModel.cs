﻿using System;

namespace DelegatesFileUploader.Models
{
    public class DelegatesViewModel
    {
        public bool Init { get; set; } = true;

        public string SuccessfulMessage { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime? LastChangeDate { get; set; }
    }
}
