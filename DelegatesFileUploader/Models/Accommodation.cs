﻿namespace DelegatesFileUploader.Models
{
    public class Accommodation
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string Address { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public AccommodationTypes Type { get; set; }
    }
}
