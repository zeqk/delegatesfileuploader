﻿namespace DelegatesFileUploader.Models
{
    public enum FlightStatuses
    {
        Arribado,
        Programado,
        Cancelado,
        Despegado,
        Incidente,
        Desviado,
        Redirigido,
        Desconocido
    }
}
