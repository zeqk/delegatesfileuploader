﻿namespace DelegatesFileUploader.Models
{
    public class SpecialRooming
    {
        public string DelegateName { get; set; }

        public long ApplicantNo { get; set; }

        public long GroupNo { get; set; }

        public string BranchName { get; set; }

        public string AccommodationName { get; set; }

        public string AccomodationAddress { get; set; }

        public string AccommodationCoordinates { get; set; }
    }
}
