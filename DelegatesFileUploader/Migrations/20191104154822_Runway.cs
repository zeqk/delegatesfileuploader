﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class Runway : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ActualDateTime",
                table: "Fligths",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ActualRunway",
                table: "Fligths",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EstimatedRunway",
                table: "Fligths",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActualDateTime",
                table: "Fligths");

            migrationBuilder.DropColumn(
                name: "ActualRunway",
                table: "Fligths");

            migrationBuilder.DropColumn(
                name: "EstimatedRunway",
                table: "Fligths");
        }
    }
}
