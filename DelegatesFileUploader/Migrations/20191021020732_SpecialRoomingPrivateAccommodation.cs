﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class SpecialRoomingPrivateAccommodation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SpecialRoomingPrivateAccomodation",
                table: "Delegates",
                newName: "SpecialRoomingPrivateAccommodation");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SpecialRoomingPrivateAccommodation",
                table: "Delegates",
                newName: "SpecialRoomingPrivateAccomodation");
        }
    }
}
