﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DelegatesFileUploader.Migrations
{
    public partial class LastChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastChangeDate",
                table: "Delegates",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastChangeDate",
                table: "Delegates");
        }
    }
}
