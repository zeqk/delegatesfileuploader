﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class FlightData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Flight",
                table: "Fligths",
                newName: "FlightNumber");

            migrationBuilder.AddColumn<long>(
                name: "ArrivalFlightDataId",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DepartureFlightDataId",
                table: "Delegates",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_ArrivalFlightDataId",
                table: "Delegates",
                column: "ArrivalFlightDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_DepartureFlightDataId",
                table: "Delegates",
                column: "DepartureFlightDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_Delegates_Fligths_ArrivalFlightDataId",
                table: "Delegates",
                column: "ArrivalFlightDataId",
                principalTable: "Fligths",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Delegates_Fligths_DepartureFlightDataId",
                table: "Delegates",
                column: "DepartureFlightDataId",
                principalTable: "Fligths",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Delegates_Fligths_ArrivalFlightDataId",
                table: "Delegates");

            migrationBuilder.DropForeignKey(
                name: "FK_Delegates_Fligths_DepartureFlightDataId",
                table: "Delegates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_ArrivalFlightDataId",
                table: "Delegates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_DepartureFlightDataId",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "ArrivalFlightDataId",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "DepartureFlightDataId",
                table: "Delegates");

            migrationBuilder.RenameColumn(
                name: "FlightNumber",
                table: "Fligths",
                newName: "Flight");
        }
    }
}
