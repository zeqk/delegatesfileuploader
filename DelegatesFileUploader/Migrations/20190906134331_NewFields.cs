﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DelegatesFileUploader.Migrations
{
    public partial class NewFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Schedule",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "IndividualDeparture",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "IndividualArrival",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ArrivalDateTime",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ApplicantStatus",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<string>(
                name: "ArrivalTransport",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DepartureDateTime",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "GroupArrivalDateTime",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "GroupDepartureDateTime",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "IndividualArrivalDateTime",
                table: "Delegates",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "IndividualDepartureDateTime",
                table: "Delegates",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArrivalTransport",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "DepartureDateTime",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "GroupArrivalDateTime",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "GroupDepartureDateTime",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "IndividualArrivalDateTime",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "IndividualDepartureDateTime",
                table: "Delegates");

            migrationBuilder.AlterColumn<int>(
                name: "Schedule",
                table: "Delegates",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "IndividualDeparture",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "IndividualArrival",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ArrivalDateTime",
                table: "Delegates",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ApplicantStatus",
                table: "Delegates",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
