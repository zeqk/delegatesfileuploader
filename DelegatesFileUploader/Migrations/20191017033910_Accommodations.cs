﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class Accommodations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Hotels");

            migrationBuilder.RenameColumn(
                name: "HotelId",
                table: "Delegates",
                newName: "AccommodationId");

            migrationBuilder.AddColumn<bool>(
                name: "SpecialRoomingPrivateAccomodation",
                table: "Delegates",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Accommodations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accommodations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Updates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DelegatesLastUpdate = table.Column<DateTime>(nullable: true),
                    FligthsLastUpdate = table.Column<DateTime>(nullable: true),
                    ActivitiesLastUpdate = table.Column<DateTime>(nullable: true),
                    SpecialRoomingLastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Updates", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Delegates_AccommodationId",
                table: "Delegates",
                column: "AccommodationId");

            migrationBuilder.CreateIndex(
                name: "IX_Accommodations_Name",
                table: "Accommodations",
                column: "Name");

            migrationBuilder.AddForeignKey(
                name: "FK_Delegates_Accommodations_AccommodationId",
                table: "Delegates",
                column: "AccommodationId",
                principalTable: "Accommodations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql("INSERT INTO Updates (DelegatesLastUpdate, FligthsLastUpdate, ActivitiesLastUpdate, SpecialRoomingLastUpdate) VALUES(NULL, NULL, NULL, NULL)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Delegates_Accommodations_AccommodationId",
                table: "Delegates");

            migrationBuilder.DropTable(
                name: "Accommodations");

            migrationBuilder.DropTable(
                name: "Updates");

            migrationBuilder.DropIndex(
                name: "IX_Delegates_AccommodationId",
                table: "Delegates");

            migrationBuilder.DropColumn(
                name: "SpecialRoomingPrivateAccomodation",
                table: "Delegates");

            migrationBuilder.RenameColumn(
                name: "AccommodationId",
                table: "Delegates",
                newName: "HotelId");

            migrationBuilder.CreateTable(
                name: "Hotels",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hotels", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Hotels_Name",
                table: "Hotels",
                column: "Name");
        }
    }
}
