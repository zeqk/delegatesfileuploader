﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DelegatesFileUploader.Migrations
{
    public partial class HotelReference : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FlightNumber",
                table: "Fligths",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Airport",
                table: "Fligths",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Airline",
                table: "Fligths",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HotelReference",
                table: "Delegates",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fligths_Airline",
                table: "Fligths",
                column: "Airline");

            migrationBuilder.CreateIndex(
                name: "IX_Fligths_Airport",
                table: "Fligths",
                column: "Airport");

            migrationBuilder.CreateIndex(
                name: "IX_Fligths_FlightHour",
                table: "Fligths",
                column: "FlightHour");

            migrationBuilder.CreateIndex(
                name: "IX_Fligths_FlightNumber",
                table: "Fligths",
                column: "FlightNumber");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Fligths_Airline",
                table: "Fligths");

            migrationBuilder.DropIndex(
                name: "IX_Fligths_Airport",
                table: "Fligths");

            migrationBuilder.DropIndex(
                name: "IX_Fligths_FlightHour",
                table: "Fligths");

            migrationBuilder.DropIndex(
                name: "IX_Fligths_FlightNumber",
                table: "Fligths");

            migrationBuilder.DropColumn(
                name: "HotelReference",
                table: "Delegates");

            migrationBuilder.AlterColumn<string>(
                name: "FlightNumber",
                table: "Fligths",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Airport",
                table: "Fligths",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Airline",
                table: "Fligths",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
