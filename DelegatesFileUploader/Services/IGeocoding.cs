﻿namespace DelegatesFileUploader.Services
{
    public interface IGeocoding
    {
        (double latitude, double longitude) Geocode(string address);
    }
}
