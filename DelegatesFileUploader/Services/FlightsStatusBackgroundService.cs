﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Services
{
    public class FlightsStatusBackgroundService : BackgroundService, IHostedService
    {
        public IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger _logger;
        private int _minutes = 0;

        public FlightsStatusBackgroundService(IServiceScopeFactory serviceScopeFactory
            , ILogger<FlightsStatusBackgroundService> logger
            , IConfiguration configuration)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
            _minutes = configuration.GetValue<int>("REFRESH_FLIGHTS_MINS");
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var updater = scope.ServiceProvider.GetRequiredService<IFlightUpdater>();

                int executionCount = 0;

                while (!stoppingToken.IsCancellationRequested)
                {
                    executionCount++;

                    _logger.LogInformation(
                        "{Date} Scoped Processing Service is working. Count: {Count}", DateTime.Now, executionCount);

                    await updater.UpdateFlightsAsync(DateTime.Today);

                    await Task.Delay(TimeSpan.FromMinutes(_minutes), stoppingToken);
                }

                    
            }
        }

    }
}
