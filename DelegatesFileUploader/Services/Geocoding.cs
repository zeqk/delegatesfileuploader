﻿using Microsoft.Extensions.Configuration;
using OpenCage.Geocode;
using System;
using System.Linq;

namespace DelegatesFileUploader.Services
{
    public class Geocoding : IGeocoding
    {
        private readonly Geocoder _geocoder;
        private readonly IConfiguration _configuration;
        public Geocoding(IConfiguration configuration)
        {
            _configuration = configuration;
            _geocoder = new Geocoder(configuration.GetValue<string>("ApiKey"));
        }

        public (double latitude, double longitude) Geocode(string address)
        {
            if (!String.IsNullOrEmpty(_configuration.GetValue<string>("ApiKey")))
            {
                var result = _geocoder.Geocode(address);
                if (result.Results.Length > 0)
                {
                    var geometry = result.Results.FirstOrDefault().Geometry;
                    return (geometry.Latitude, geometry.Longitude);
                }
            }

            return (0,0);
        }
    }
}
