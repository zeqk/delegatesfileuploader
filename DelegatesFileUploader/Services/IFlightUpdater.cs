﻿using System;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Services
{
    public interface IFlightUpdater
    {
        Task UpdateFlightsAsync(DateTime initialDate, bool onlyUpdateSchedule = false);
    }
}