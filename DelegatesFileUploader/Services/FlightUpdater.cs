﻿using DelegatesFileUploader.Convertions;
using DelegatesFileUploader.Data;
using DelegatesFileUploader.Models;
using DelegatesFileUploader.Models.AviationEdge;
using Microsoft.Extensions.Logging;
using Sentry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DelegatesFileUploader.Services
{
    public class FlightUpdater : IFlightUpdater
    {
        private readonly ILogger _logger;
        private readonly IAviationEdgeClient _aviationEdgeClient;
        private readonly DelegatesContext _delegatesContext;
        private readonly IHub _hub;

        public FlightUpdater(IAviationEdgeClient aviationEdgeClient
            , DelegatesContext delegatesContext
            , ILogger<FlightUpdater> logger
            , IHub hub)
        {
            //hub.CaptureEvent(new SentryEvent() { })
            _aviationEdgeClient = aviationEdgeClient;
            _delegatesContext = delegatesContext;
            _logger = logger;
            _hub = hub;
        }

        public async Task UpdateFlightsAsync(DateTime initialDate, bool updateScheduleTimeMode = false)
        {
            try
            {
                var laterDay = initialDate.AddDays(1);
                var previousDay = initialDate.AddDays(-1);
                var direction = FlightDirections.Arrival;
                if (initialDate > new DateTime(2019, 12, 14))
                    direction = FlightDirections.Departure;

                //Busco los vuelos que todavia no hayan despegado
                var fligths = _delegatesContext.Fligths
                    .Where(f => f.FlightDateTime.Date >= previousDay && f.FlightDateTime.Date < laterDay);

                
                if (direction == FlightDirections.Arrival)
                    fligths = fligths.Where(f => f.FlightStatus != FlightStatuses.Arribado);
                else
                    fligths = fligths.Where(f => f.FlightStatus != FlightStatuses.Despegado);

                if (fligths.Count() > 0)
                {
                    _logger.LogInformation("Getting timetables for {FlightCount}", fligths.Count());

                    var airports = fligths.GroupBy(f => f.Airport).ToList();


                    var timetablesByAirports = new Dictionary<string, IList<FlightTimetable>>();
                    foreach (var airport in airports)
                    {
                        var iataCode = Utils.GetIATACode(airport.Key);

                        var timetable = await _aviationEdgeClient.GetTimetablesAsync(iataCode, direction);
                        
                        timetablesByAirports.Add(iataCode, timetable);
                    }


                    foreach (var airport in airports)
                    {
                        var iataCode = Utils.GetIATACode(airport.Key);
                        var timetables = timetablesByAirports[iataCode];

                        if (timetables != null)
                        {

                            foreach (var flight in airport)
                            {
                                var day = flight.FlightDateTime.Date == initialDate.Date ? DateTime.Today :
                                    (flight.FlightDateTime.Date == previousDay.Date ? DateTime.Today.AddDays(-1) : DateTime.Today.AddDays(1));

                                var fsQuery = timetables
                                    .Where(t => t.Flight?.IataNumber == flight.FlightNumber);

                                if (direction == FlightDirections.Arrival)
                                    fsQuery = fsQuery.Where(t => t.Arrival.ScheduledTime.HasValue && 
                                                                t.Arrival.ScheduledTime.Value.Date.Equals(day));
                                else
                                    fsQuery = fsQuery.Where(t => t.Departure.ScheduledTime.HasValue && 
                                                                t.Departure.ScheduledTime.Value.Date.Equals(day));

                                var timetable = fsQuery.FirstOrDefault();

                                if(timetable != null) 
                                {
                                    var scheduleDateTime = direction == FlightDirections.Arrival ?
                                        timetable.Arrival.ScheduledTime.Value : timetable.Departure.ScheduledTime.Value;

                                    if (scheduleDateTime.Hour != flight.FlightDateTime.Hour || scheduleDateTime.Minute != flight.FlightDateTime.Minute)
                                    {
                                        var message = $"Se actualiza el horario del vuelo {flight.FlightNumber} ({flight.FlightDateTime.ToString("dd-MM-yyyy")}). " +
                                            $"Hora provista por el delegado {flight.FlightDateTime.Hour}:{flight.FlightDateTime.Minute}. " +
                                            $"Hora programada por la aerolínea {scheduleDateTime.Hour}:{scheduleDateTime.Minute}";
                                        _logger.LogInformation(message);
                                        _hub.CaptureMessage(message);
                                    }
                                                                        
                                    flight.FlightDateTime = new DateTime(flight.FlightDateTime.Year, flight.FlightDateTime.Month,
                                        flight.FlightDateTime.Day, scheduleDateTime.Hour, scheduleDateTime.Minute, scheduleDateTime.Second);
                                    flight.FlightHour = scheduleDateTime.Hour;

                                    if (!updateScheduleTimeMode)
                                    {

                                        flight.Status = timetable.Status;
                                        flight.FlightStatus = Utils.GetFlightStatuses(timetable.Status);

                                        flight.EstimatedDateTime = direction == FlightDirections.Arrival ?
                                            timetable?.Arrival.EstimatedTime : timetable?.Departure.EstimatedTime;

                                        flight.ActualDateTime = direction == FlightDirections.Arrival ?
                                            timetable?.Arrival.ActualTime : timetable?.Departure.ActualTime;

                                        flight.EstimatedRunway = direction == FlightDirections.Arrival ?
                                            timetable?.Arrival.EstimatedRunway : timetable?.Departure.EstimatedRunway;

                                        flight.ActualRunway = direction == FlightDirections.Arrival ?
                                            timetable?.Arrival.ActualRunway : timetable?.Departure.ActualRunway;
                                    }

                                    

                                    flight.UpdateStatusDateTime = DateTime.Now;

                                    await _delegatesContext.SaveChangesAsync();
                                }
                            }
                        }
                    }

                    await _delegatesContext.UpdateFligthsDelayedMinutesAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}
