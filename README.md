# DOCKER COMPOSE

```yml
  delegatesfileuploader:
    container_name: uploader
    image: registry.gitlab.com/zeqk/delegatesfileuploader/delegatesfileuploader:latest
    ports:
      - 8085:80
    restart: always
    links:
      - mariadb:db
    environment:
      - UploadPassword=****
      - ConnectionStrings:Default=server=db;port=3306;Database=delegates;username=****;password=***

```